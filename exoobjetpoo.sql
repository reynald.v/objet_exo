-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 22 Mars 2017 à 23:18
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `exoobjetpoo`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(200) NOT NULL,
  `titre` varchar(200) NOT NULL,
  `contenu` text NOT NULL,
  `date` date NOT NULL,
  `auteur` int(11) UNSIGNED NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`auteur`),
  KEY `user_id_2` (`auteur`),
  KEY `user_id_3` (`auteur`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `slug`, `titre`, `contenu`, `date`, `auteur`, `image`) VALUES
(3, 'slugarticle1', 'article 1', 'Pellentesque consectetur venenatis diam. Vivamus sit amet eros purus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nunc tincidunt lacus purus, ac dignissim nisi mattis id. Etiam eget purus augue. Duis hendrerit, urna ac rhoncus dapibus, eros tortor sodales justo, a ultrices eros libero ac risus. Nulla facilisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-03-09', 1, 'photo2.jpg'),
(4, 'slugarticle2', 'article 2', 'Etiam eget purus augue. Duis hendrerit, urna ac rhoncus dapibus, eros tortor sodales justo, a ultrices eros libero ac risus. Nulla facilisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-03-10', 1, ''),
(5, 'slugarticle3', 'article 3', 'Etiam eget purus augue. Duis hendrerit, urna ac rhoncus dapibus, eros tortor sodales justo, a ultrices eros libero ac risus. Nulla facilisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-03-10', 1, ''),
(6, 'slugarticle4', 'article 4', 'Etiam eget purus augue. Duis hendrerit, urna ac rhoncus dapibus, eros tortor sodales justo, a ultrices eros libero ac risus. Nulla facilisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-03-10', 1, ''),
(7, 'slugarticle5', 'article 5', 'Etiam eget purus augue. Duis hendrerit, urna ac rhoncus dapibus, eros tortor sodales justo, a ultrices eros libero ac risus. Nulla facilisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-03-10', 2, ''),
(8, 'slugarticle6', 'article 6', 'Etiam eget purus augue. Duis hendrerit, urna ac rhoncus dapibus, eros tortor sodales justo, a ultrices eros libero ac risus. Nulla facilisi. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-03-10', 2, ''),
(12, 'test-ultime', 'TEST ULTIME', 'Lorem ripfdd df sdf sf sdf s g d vdcscsd', '2017-03-17', 1, 'sdsfsf dfdfdfd fdfd scs'),
(39, 'test', 'TEST', 'Donec vel rutrum ante, eget tincidunt ante. Maecenas eu risus eu lectus dignissim mollis. Nulla id ultricies ex. Aliquam venenatis sagittis dolor vel feugiat. Suspendisse aliquam ligula dolor, non sollicitudin elit porttitor at. In nec augue justo. Duis eget malesuada metus, et tempus quam.', '2017-03-20', 1, '1caf28732d3fbfa8e9669c6d1ebb1da1369528aa.png'),
(40, 'test-ultime-4', 'TEST ULTIME 4', 'Donec vel rutrum ante, eget tincidunt ante. Maecenas eu risus eu lectus dignissim mollis. Nulla id ultricies ex. Aliquam venenatis sagittis dolor vel feugiat. Suspendisse aliquam ligula dolor, non sollicitudin elit porttitor at. In nec augue justo. Duis eget malesuada metus, et tempus quam.', '2017-03-20', 1, 'e469f1ef92aa91ba8832d21bc616112a7cae0d2e.png'),
(41, 'test-ultime-final', 'TEST ULTIME FINAL', 'Donec vel rutrum ante, eget tincidunt ante. Maecenas eu risus eu lectus dignissim mollis. Nulla id ultricies ex. Aliquam venenatis sagittis dolor vel feugiat. Suspendisse aliquam ligula dolor, non sollicitudin elit porttitor at. In nec augue justo. Duis eget malesuada metus, et tempus quam.', '2017-03-20', 1, '0ab001b747043a030c111da2ea6628dda0520bae.png'),
(42, 'test-ajout-article-22-03', 'TEST AJOUT ARTICLE 22-03', 'Nulla eget ligula sit amet mauris placerat luctus vel vitae nisi. Morbi ultrices nisi et ante vestibulum, nec dictum nisi fringilla. Ut nec lectus pretium ligula efficitur posuere. Donec sit amet leo sit amet sapien porta dictum. Phasellus vitae purus id magna elementum auctor. Duis laoreet purus varius vulputate vestibulum. Proin ultrices molestie urna eget ultrices. Suspendisse porta orci justo, ut sagittis dui feugiat ac. Aliquam placerat efficitur ante, eget scelerisque turpis. Vivamus egestas tortor et semper imperdiet. ', '2017-03-22', 1, '31af5a7cbdfcbdabdba75c1b32c174eda5a335da.png'),
(43, 'test-ajout-article-22-03-bis', 'TEST AJOUT ARTICLE 22-03 BIS', 'Donec vel rutrum ante, eget tincidunt ante. Maecenas eu risus eu lectus dignissim mollis. Nulla id ultricies ex. Aliquam venenatis sagittis dolor vel feugiat. Suspendisse aliquam ligula dolor, non ', '2017-03-22', 1, 'd6322fa0337fae48747fcbce3587eb83ac877e95.png');

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `titre`, `slug`) VALUES
(1, 'Categorie1', 'slugCategorie1'),
(2, 'Categorie2', 'slugCategorie2');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `alt` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id`, `url`, `alt`) VALUES
(1, 'gorilla.jpg', 'un gorille'),
(2, 'photo1.jpg', 'un deuxieme gorille');

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auteur` varchar(60) NOT NULL,
  `valeur` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `produit` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produit` (`produit`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `notes`
--

INSERT INTO `notes` (`id`, `auteur`, `valeur`, `date`, `produit`) VALUES
(1, 'auteur1', 1, '2017-03-13', 3),
(2, 'auteur2', 4, '2017-03-13', 5),
(3, 'auteur3', 2, '2017-03-13', 4),
(4, 'auteur2', 4, '2017-03-14', 5),
(5, 'test note 2', 3, '2017-03-13', 5),
(6, 'test note final', 4, '2017-03-13', 5),
(7, 'test note final 2', 3, '2017-03-13', 5),
(8, 'ffdfdfdgdgfd', 3, '2017-03-13', 3),
(9, 'ffdfdfdgdgfd', 4, '2017-03-13', 3),
(10, 'salut c''est moi', 4, '2017-03-14', 4),
(11, 'test note 4', 3, '2017-03-14', 3),
(12, 'test note 4', 3, '2017-03-14', 3),
(13, 'test note 4', 3, '2017-03-14', 3),
(14, 'test note 5', 3, '2017-03-14', 3),
(15, 'test note 3', 3, '2017-03-14', 5),
(16, 'ddsdsd', 5, '2017-03-14', 5),
(17, 'note aaaaa', 3, '2017-03-14', 5),
(18, 'note aaaaa', 3, '2017-03-14', 5),
(19, '444', 3, '2017-03-14', 5),
(20, '444', 4, '2017-03-14', 5),
(21, '444', 4, '2017-03-14', 5),
(22, '444', 4, '2017-03-14', 5),
(23, '444', 4, '2017-03-14', 5),
(24, '444', 4, '2017-03-14', 5),
(25, '444', 3, '2017-03-14', 5),
(26, '444', 3, '2017-03-14', 5),
(27, '444', 3, '2017-03-14', 5),
(28, 'hello', 3, '2017-03-14', 5),
(29, 'hello', 3, '2017-03-14', 5),
(30, 'hello', 3, '2017-03-14', 5),
(31, 'hello', 3, '2017-03-14', 5),
(32, 'hello', 3, '2017-03-14', 5),
(33, 'hello', 3, '2017-03-14', 5),
(34, 'hello', 3, '2017-03-14', 5),
(35, 'hello', 3, '2017-03-14', 5),
(36, 'dkfdf', 4, '2017-03-14', 5),
(37, 'dkfdf', 4, '2017-03-14', 5),
(38, 'dkfdf', 3, '2017-03-14', 5),
(39, 'dkfdf', 3, '2017-03-14', 5),
(40, 'dkfdf', 3, '2017-03-14', 5),
(41, 'sdsfsfs', 3, '2017-03-14', 5),
(42, 'sdsfsfs', 3, '2017-03-14', 5),
(43, 'sdsfsfs', 3, '2017-03-14', 5),
(44, 'sdsfsfs', 3, '2017-03-14', 5),
(45, 'sddffdfd', 4, '2017-03-14', 5),
(46, 'TEST', 4, '2017-03-14', 5),
(47, 'TEST2', 3, '2017-03-14', 5),
(48, 'TEST2', 3, '2017-03-14', 5),
(49, 'TEST2', 4, '2017-03-14', 3),
(50, 'TEST 5', 3, '2017-03-14', 3),
(51, 'ddddd', 3, '2017-03-14', 5),
(52, 'sjhfjdffd', 3, '2017-03-15', 3),
(53, 'sjhfjdffd', 3, '2017-03-15', 3),
(54, 'sjhfjdffd', 3, '2017-03-15', 3),
(55, 'fkdhdfhdf', 4, '2017-03-15', 3),
(56, 'sdsdsdssd', 3, '2017-03-15', 3),
(57, 'sdsdsdssd', 3, '2017-03-15', 3),
(58, 'ddddd', 3, '2017-03-15', 3),
(59, 'test45', 5, '2017-03-15', 3),
(60, 'test 45 2', 3, '2017-03-15', 3),
(61, '4zjfdhksh', 3, '2017-03-15', 3),
(62, 'eeeee', 4, '2017-03-15', 3),
(63, 'ddddd', 4, '2017-03-15', 3),
(64, 'dddd', 3, '2017-03-15', 3),
(65, 'dddd', 3, '2017-03-15', 3),
(66, 'dddd', 3, '2017-03-15', 3),
(67, 'ddd', 5, '2017-03-15', 3),
(68, 'dddd', 4, '2017-03-15', 3),
(69, 'ssdd', 4, '2017-03-15', 3),
(70, 'ddddd', 3, '2017-03-15', 3),
(71, 'xxxxxxx', 4, '2017-03-15', 3),
(72, 'ddd', NULL, '2017-03-15', 4),
(73, 'ddddf', NULL, '2017-03-15', 3),
(74, 'dddffgg', NULL, '2017-03-15', 3),
(75, 'ddddd', NULL, '2017-03-15', 3),
(76, 'dddd', NULL, '2017-03-15', 3),
(77, 'dddd', 3, '2017-03-15', 3),
(78, 'sfffrrr', NULL, '2017-03-15', 3),
(79, 'sddfdf', NULL, '2017-03-15', 3),
(80, 'dddddd', NULL, '2017-03-15', 3),
(81, 'ddddddffff', 4, '2017-03-15', 3),
(82, 'xcsfssf', 5, '2017-03-15', 4),
(83, 'xfsfsf', 4, '2017-03-22', 5);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `contenu` text NOT NULL,
  `image` int(10) UNSIGNED DEFAULT NULL,
  `prix` decimal(10,0) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `categorie` int(11) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categorie` (`categorie`),
  KEY `categorie_2` (`categorie`),
  KEY `image` (`image`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`id`, `titre`, `contenu`, `image`, `prix`, `slug`, `categorie`) VALUES
(3, 'produit1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod suscipit libero, in sollicitudin enim mattis non. In auctor lacus enim, sit amet dignissim dolor condimentum quis. Donec vestibulum posuere leo, id consequat ex ultricies non. Vivamus placerat sollicitudin tortor a ullamcorper. Quisque a ex vel turpis finibus varius. Proin varius accumsan lorem, quis commodo tortor auctor sed. Curabitur in dolor iaculis, sagittis arcu a, auctor nisl. Donec ex nulla, aliquam vitae commodo auctor, gravida sed neque.', 1, '10', 'slugProduit1', 2),
(4, 'produit2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla euismod suscipit libero, in sollicitudin enim mattis non. In auctor lacus enim, sit amet dignissim dolor condimentum quis. Donec vestibulum posuere leo, id consequat ex ultricies non. Vivamus placerat sollicitudin tortor a ullamcorper. Quisque a ex vel turpis finibus varius. Proin varius accumsan lorem, quis commodo tortor auctor sed. Curabitur in dolor iaculis, sagittis arcu a, auctor nisl. Donec ex nulla, aliquam vitae commodo auctor, gravida sed neque.', 2, '20', 'slugProduit2', 1),
(5, 'produit3', 'Fusce sodales metus vitae nibh commodo, at pretium sem iaculis. Vestibulum vel lorem eget tellus sagittis aliquet. Pellentesque fringilla lacus eu ex dignissim pharetra. Sed placerat ornare massa, consectetur placerat lectus pharetra vitae. Etiam vitae lacus vel mauris finibus commodo quis dignissim elit. Aenean dapibus sollicitudin felis quis commodo. Nunc rutrum sem vitae ante pellentesque ornare.', 2, '10', 'produit', 2),
(6, 'produit4', 'Fusce soare massa, consectetur placerat lectus pharetra vitae. Etiam vitae lacus vel mauris finibus commodo quis dignissim elit. Aenean dapibus sollicitudin felis quis commodo. Nunc rutrum sem vitae ante pellentesque ornare.', 2, '52', 'produit', 1),
(7, 'TEST AJOUT PROD 02', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras aliquam augue in nunc fringilla, ac volutpat mi auctor. In eu erat elit. Praesent porttitor efficitur neque, nec dignissim nisl. Aliquam imperdiet orci eget nisl semper, id ultrices augue iaculis. Nulla vitae viverra metus. Phasellus maximus sapien eros, at hendrerit purus rhoncus fermentum. In dignissim vestibulum mi eu euismod. Etiam vitae molestie tortor. Ut venenatis justo at eros pharetra, et tempus risus finibus. Ut elementum tristique magna non ullamcorper.', 2, NULL, 'test-ajout-prod-02', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `privilege` tinyint(4) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `privilege`, `email`) VALUES
(1, 'Reynald', '$2y$10$fHzUJdd201V3GHulj5RXrewfPsr2fCrX8q768mAGvuy9gc0L3m/HC', 4, 'reynald.v@free.fr'),
(2, 'duchmol', '$2y$10$fHzUJdd201V3GHulj5RXrewfPsr2fCrX8q768mAGvuy9gc0L3m/HC', 1, 'machin@free.fr');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`auteur`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`produit`) REFERENCES `produit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`categorie`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `produit_ibfk_2` FOREIGN KEY (`image`) REFERENCES `image` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
