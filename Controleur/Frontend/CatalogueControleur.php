<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controleur\Frontend;

/**
 * Description of CatalogueControleur
 *
 * @author Human Booster
 */
class CatalogueControleur extends \Lib\backControleur {

    protected function indexAction() {
        echo 'je suis le catalogueControleur';
        $categorieManager = new \modele\CategorieManager();
        $categories = $categorieManager->getAllCategoriesWithProduit();

        //var_dump($categories);
        $this->render('catalogue/index.html.php', ['mes_categories' => $categories]);
//    $this->render('catalogue/index.html.php', ['mes_categories' => $categories, 'titre' => 'Home Catalogue']);
    }

    public function categorieAction() {
        $id = $_GET['id'];
        $produitManager = new \modele\ProduitManager();
        $produits = $produitManager->getProduitByCategorie($id);
        $categorieManager = new \modele\CategorieManager();
        $categories = $categorieManager->getAllCategoriesWithProduit();
        $this->render('catalogue/categorie.html.php', ['produits' => $produits, 'mes_categories' => $categories]); //2eme argument doit etre un tableau (voir methode render dans backcontroleur ) on peut rajouter d'autre valeur dans ce tableau
        //var_dump($produits);
    }

    public function produitAction() {
        $id = $_GET['id'];
        $produitManager = new \modele\ProduitManager;
        $nm = new \modele\NotesManager();
        $produit = $produitManager->getProduitByid($id);
        $notes = $nm->getNotesByProduitId($id);
        $categorieManager = new \modele\CategorieManager();
        $categories = $categorieManager->getAllCategoriesWithProduit();

        //var_dump($notes);
        $this->render('catalogue/detail.html.php', ['produit' => $produit, 'notes' => $notes, 'mes_categories' => $categories, 'titre' => 'Home Catalogue']); //2eme argument doit etre un tableau (voir methode render dans backcontroleur ) on peut rajouter d'autre valeur dans ce tableau
        //var_dump($produit);
    }

    public function allproduitAction() {
//        if (isset($_GET['page'])) {
//            $page = $_GET['page'];
//        } else {
//            $page = 1;
//        }
//        $limit = 5;
//        $offset = ($page - 1) * $limit;
        $produit = new \modele\Produit; // clic droit fix uses pour utiliser les use pour éviter les backslash
        $produitManager = new \modele\ProduitManager;
        $produits = $produitManager->getAllProduits(0, 50);
//        $total = $produitManager->getTotalProduits();
//        $pages = ceil($total / $limit);
        $categorieManager = new \modele\CategorieManager();
        $categories = $categorieManager->getAllCategoriesWithProduit();
        // var_dump($produits);
        $this->render('catalogue/allproduit.html.php', ['all_produit' => $produits, 'mes_categories' => $categories, 'titre' => 'TOUS LES ARTICLES']);
    }

    public function noteAction() {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $nm = new \modele\NotesManager();
            $notes = new \modele\Notes($_POST); //créer un nouvel objet notes
            if (!isset($_POST['valeur'])) {//on le fait direct ici car les bouton radio non coché ne retourne pas de clé ni de valeur
                $notes->setErreur("Donner une note SVP !");
            }
            $pm = new \modele\ProduitManager();
            $produit = $pm->getProduitById($notes->getProduit());
            if ($notes->getErreur() == []) {
                $nm->insertNote($notes);

                //var_dump($produit);
                $success = '<p class="alert alert-success" style="border-radius: 5px;padding:15px;color:green;"><strong>enregistrement réussi</strong></p>';
            } else {
                //var_dump($notes->getErreur());
                $success = '<p class="alert alert-danger" style="border-radius: 5px;padding:15px;color:red;"><strong>' . implode(', ', $notes->getErreur()) . '</strong></p>';
            }
            \Lib\backControleur::setFlash($success);
            header('Location:/OBJET_EXO/catalogue/produit/' . $produit->getSlug() . '-' . $produit->getId());
            //var_dump($success);
            exit();
        }
    }

}
