<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controleur\Backend;

use Lib\backControleur;
use modele\Image;
use modele\ImageManager;

/**
 * Description of ConnexionControleur
 *
 * @author Human Booster
 */
class ImageControleur extends backControleur {

    public function indexAction() {//module = index
        echo 'je suis le ImageControleur';
    }

    public function ajoutAction() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

//            $image = new \modele\image($_FILES['image']['name']); //objet image dans lequel on mets les données POST
//            var_dump($_FILES['image']['name']);
            $image = new \modele\Image($_FILES); //objet image dans lequel on mets les données POST
            //var_dump($image);
            if ($image->getErreur() == []) {
                if (($_FILES['image']['error']) != 4) {
                    $upload = $image->upload(__DIR__ . '/../../Web/images', $_FILES['image']);
                    var_dump($upload);
                    if ($upload['upload']) {
                        $mini = $image->minImg(__DIR__ . '/../../Web/images/' . $upload['message'], __DIR__ . '/../../Web/images/thumbnails');
                        $image->setUrl($upload['message']);
                    } else {
                        $this->setFlash($upload['message']);
                    }
                }
//                $image->setAlt($image->getUrl());
                $image->setAlt(Image::slugify($image->getUrl()));
//                $image->setUrl($image->getUrl());
                ////$image->setId($image->getId());
                //$produit->setAuteur($this->app->getUser());
                $im = new ImageManager();
                var_dump($image);
                if ($im->ajoutImage($image)) {


                    $this->setFlash('Image enregistré');
                    //$this->render('admin/produit/ajout.html.php'); //methode  = article
                    //header('Location: ' . \Lib\Application::REP_RACINE . 'admin?module=produit&methode=ajout&imageId=' . $image->getId());
                    header('Location: ' . \Lib\Application::REP_RACINE . 'admin?module=produit&methode=ajout');
                    exit();
                } else {
                    $this->setFlash('Erreur BDD Image');
                }
                //mode pour rediriger vers l'article enregistrer
//                if ($id = $am->ajoutArticle($article)) {
//                    $this->setFlash('Article enregistré');
//                    $article -> setId($id);
//                } else {
//                    $this->setFlash('Erreur BDD');
//                }
//
//                header('Location: '.\Lib\Application::REP_RACINE.'admin?methode=ajout');
//                    exit();
            } else {

                $this->setFlash(implode('<br>', $image->getErreur()));
            }
        }

        $this->render('admin/image/ajout.html.php');
    }

}
