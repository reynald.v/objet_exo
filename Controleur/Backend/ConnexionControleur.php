<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controleur\Backend;

use Lib\backControleur;

/**
 * Description of ConnexionControleur
 *
 * @author Human Booster
 */
class ConnexionControleur extends backControleur {

    public function indexAction() {
        $token = sha1(uniqid(rand(), true));
        $tokenTime = time();
        $_SESSION['token'] = $token;
        $_SESSION['tokenTime'] = $tokenTime;
        echo 'je suis le ConnexionControleur';
        $this->render('admin/connexion.html.php', ['token' => $token]);
    }

    public function loginAction() {
        if (((($_POST["token"]) == $_SESSION['token']) && ((time() - $_SESSION['tokenTime']) <= 300))) { //5mn //si jeton est bon
            $um = new \modele\UserManager();
            $login = $_POST['login'];
            $pwd = $_POST['pwd'];
            $user = $um->getUserByLogin($login);
            //var_dump($user);

            if ($user == false) {
                sleep(1); // pour ralentir les robots
                $this->setFlash("Le login n'existe pas");
            } else {
                $hash = $user->getPassword();
                //var_dump($hash);
                if (password_verify($pwd, $hash) == false) {
                    sleep(1); // pour ralentir les robots et éviter les attaques de force brut : à placer avant erreur sauf jeton qui n"est

                    $this->setFlash('mauvais mot de passe');
                } else {
                    $_SESSION['IPaddress'] = sha1($_SERVER['REMOTE_ADDR']);
                    $_SESSION['userAgent'] = sha1($_SERVER['HTTP_USER_AGENT']); //ici on stocke ces variables
                    $user->setAuthentifie(true);
                    $_SESSION['user'] = $user;
                    //$this->setFlash('OK');
                    //setcookie(session_name(), session_id(), time() + 3600, '/', null, null, true);
                    setcookie(session_name(), session_id(), time() + 3600, '/', null, null, true); //ici on rend la session valable 1 heure seulement à partir du dossier admin et pas du dossier racine (on a remplacé le / par NULL après times()
                    if (!$user->isAdmin()) {
                        $this->setFlash("Vous n'avez pas les droits !!");
                    }
                }
            }
        } else {

            $this->setFlash('Mauvais Token');
            // var_dump($success);
        }
        header('Location: ' . \Lib\Application::REP_RACINE . 'admin');
        exit();
    }

    public function logOutAction() {
        session_destroy();
        setcookie(session_name(), session_id(), time() - 10, '/', null, null, true);
        header('Location: ' . \Lib\Application::REP_RACINE . 'admin');
        exit();
    }

}
