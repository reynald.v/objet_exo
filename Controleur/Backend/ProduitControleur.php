<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controleur\Backend;

use Lib\backControleur;
use modele\Produit;
use modele\ProduitManager;

/**
 * Description of ConnexionControleur
 *
 * @author Human Booster
 */
class ProduitControleur extends backControleur {

    public function indexAction() {//module = index
        echo 'je suis le ProduitControleur';
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $limit = 10;
        $offset = ($page - 1) * $limit;
        $produit = new Produit; // clic droit fix uses pour utiliser les use pour éviter les backslash
        $produitManager = new ProduitManager;
        $produits = $produitManager->getAllProduits($offset, $limit);
        $total = $produitManager->getTotalProduits();

        $pages = ceil($total / $limit);

        $this->render('admin/produit/index.html.php', ['all_produits' => $produits, 'page' => $page, 'pages' => $pages]); //methode  = article
        //var_dump($produits);
    }

    public function ajoutAction() {
        //$imageId = $_GET['imageId'];
        $cm = new \modele\CategorieManager();
        $categories = $cm->getAllCategories();
        //var_dump($categories);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $produit = new Produit($_POST); //objet produit dans lequel on mets les données POST

            if ($produit->getErreur() == []) {
                if (($_FILES['image']['error']) != 4) {
                    $image = new \modele\Image($_FILES);
                    $upload = $image->upload(__DIR__ . '/../../Web/images/upload', $_FILES['image']);
                    //var_dump($upload);
                    if ($upload['upload']) {
                        $mini = $image->minImg(__DIR__ . '/../../Web/images/upload/' . $upload['message'], __DIR__ . '/../../Web/images/thumbnails');

                        //var_dump($_FILES['image']);
                        $image->setUrl($upload['message']);
                        $image->setMiniature($mini);
                        $image->setAlt(\modele\Image::slugify($image->getUrl()));
                        $im = new \modele\ImageManager();
                        if ($im->ajoutImageProd($image)) {

//                        $article->setImage_originale($upload['message']);
                            $this->setFlash('<div class="alert alert-success">Image enregistré</div>');
                        } else {
                            $this->setFlash('<div class="alert alert-danger" style="background-color: pink;">Erreur BDD Image</div>');
                        }
//                       $produit->setImage($mini);
//                        $produit->setImage_originale($upload['message']);
                    } else {
                        $this->setFlash($upload['message']);
                    }
                }
                $im = new \modele\ImageManager();
                //$img = $im->getLastIdImage();
                $produit->setSlug(Produit::slugify($produit->getTitre()));
                $produit->setImage($im->getLastIdImage());

                $produit->setAuteur($this->app->getUser());
                $pm = new ProduitManager();
                if ($pm->ajoutProduit($produit)) {
                    $this->setFlash('<div class="alert alert-success">Produit enregistré</div>');
                } else {
                    $this->setFlash('<div class="alert alert-danger" style="background-color: pink;">Erreur BDD</div>');
                }
                //header('Location: ' . \Lib\Application::REP_RACINE . 'admin');
                //exit();
                //mode pour rediriger vers l'article enregistrer
//                if ($id = $am->ajoutArticle($article)) {
//                    $this->setFlash('Article enregistré');
//                    $article -> setId($id);
//                } else {
//                    $this->setFlash('Erreur BDD');
//                }
//
//                header('Location: '.\Lib\Application::REP_RACINE.'admin?methode=ajout');
//                    exit();
            } else {

                $this->setFlash(implode('<br>', $produit->getErreur()));
            }
        }


        $this->render('admin/produit/ajout.html.php', ['categories' => $categories]);
        //var_dump($image);
    }

    public function updateAction() {
        echo 'update';
        //$imageId = $_GET['imageId'];
        $cm = new \modele\CategorieManager();
        $categories = $cm->getAllCategories();
        //var_dump($categories);
        $produitManager = new \modele\ProduitManager;
        $produit = $produitManager->getProduitById($_GET['id']);
        var_dump($produit);

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $produit = new Produit($_POST); //objet produit dans lequel on mets les données POST
            $im = new \modele\ImageManager();

//            if ($produit->getImage() !== null) {
//                $img = $im->getImageById($produit->getImage());
//                $produit->setImage($img);
//            }

            if ($produit->getErreur() == []) {
                if ((isset($_FILES)) && (($_FILES['image']['error']) != 4)) {
                    $image = new \modele\Image($_FILES);
                    $upload = $image->upload(__DIR__ . '/../../Web/images/upload', $_FILES['image']);
                    //var_dump($_FILES);
                    if ($upload['upload']) {
                        $mini = $image->minImg(__DIR__ . '/../../Web/images/upload/' . $upload['message'], __DIR__ . '/../../Web/images/thumbnails');
                        var_dump($produit);
                        //var_dump($_FILES['image']);
                        $image->setId($_POST['idImage']);
                        $image->setUrl($upload['message']);
                        $image->setMiniature($mini);
                        $image->setAlt(\modele\Image::slugify($image->getUrl()));

//                        $image = new \modele\Image();
//                        $produit->setImage($image->getId());
                        if ($im->updateImageProd($image)) {
                            var_dump($image);
//                            $produit->setImage_originale($upload['message']);
                            $this->setFlash('<div class="alert alert-success">Image enregistré</div>');
                        } else {
                            $this->setFlash('<div class="alert alert-danger" style="background-color: pink;">Erreur BDD Image</div>');
                        }
//                        $im = new \modele\ImageManager();
//                $img = $im->ajoutImageProd();
//
//                        $produit->setImage($im->updateImageProd($image->getId()));
//                       $produit->setImage($mini);
//                        $produit->setImage_originale($upload['message']);
                    } else {
                        $this->setFlash($upload['message']);
                    }
                }
//                $im = new \modele\ImageManager();
//                $image = new \modele\Image();
//                $produit->setImage($im->getLastIdImage());
                $produit->setSlug(Produit::slugify($produit->getTitre()));

                $produit->setAuteur($this->app->getUser());

                $produit->setPublier($produit->getPublier());


                $pm = new ProduitManager();
                if ($pm->updateProduit($produit)) {
                    var_dump($produit);
                    $this->setFlash('<div class="alert alert-success">Produit mis à jour</div>');
                } else {
                    $this->setFlash('<div class="alert alert-danger" style="background-color: pink;">Erreur BDD</div>');
                }
                //header('Location: ' . \Lib\Application::REP_RACINE . 'admin');
                //exit();
                //mode pour rediriger vers l'article enregistrer
//                if ($id = $am->ajoutArticle($article)) {
//                    $this->setFlash('Article enregistré');
//                    $article -> setId($id);
//                } else {
//                    $this->setFlash('Erreur BDD');
//                }
//
//                header('Location: '.\Lib\Application::REP_RACINE.'admin?methode=ajout');
//                    exit();
            } else {

                $this->setFlash(implode('<br>', $produit->getErreur()));
            }
        }


        $this->render('admin/produit/update.html.php', ['categories' => $categories, 'produit' => $produit]);
//        var_dump($categories);
//        var_dump($produit);
    }

    public function deleteAction() {

        $produitManager = new \modele\ProduitManager;
        $produit = $produitManager->getProduitById(0);
        //var_dump($article);
        $produitManager->deleteProduit($produit);


        header('Location: ' . \Lib\Application::REP_RACINE . 'admin?module=produit&methode=index');
        exit();
    }

}
