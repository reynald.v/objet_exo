<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controleur\Backend;

use Lib\backControleur;
use modele\Categorie;
use modele\CategorieManager;

/**
 * Description of ConnexionControleur
 *
 * @author Human Booster
 */
class CategorieControleur extends backControleur {

    protected function indexAction() {
        echo 'je suis le categorieControleur';
        $categorieManager = new \modele\CategorieManager();
        $categories = $categorieManager->getAllCategories();

        $this->render('admin/categorie/index.html.php', ['mes_categories' => $categories]);
//    $this->render('catalogue/index.html.php', ['mes_categories' => $categories, 'titre' => 'Home Catalogue']);
    }

    public function ajoutAction() {


        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $categorie = new \modele\categorie($_POST); //objet produit dans lequel on mets les données POST


            if ($categorie->getErreur() == []) {
                //$img = $im->getLastIdImage();
                $categorie->setSlug(\modele\categorie::slugify($categorie->getTitre()));

                var_dump($categorie);
                //$produit->setAuteur($this->app->getUser());
                $pm = new CategorieManager();
                if ($pm->ajoutCategorie($categorie)) {
                    $this->setFlash('<div class="alert alert-success">Categorie enregistré</div>');
                } else {
                    $this->setFlash('<div class="alert alert-danger" style="background-color: pink;">Erreur BDD</div>');
                }
                //header('Location: ' . \Lib\Application::REP_RACINE . 'admin');
                //exit();
                //mode pour rediriger vers l'article enregistrer
//                if ($id = $am->ajoutArticle($article)) {
//                    $this->setFlash('Article enregistré');
//                    $article -> setId($id);
//                } else {
//                    $this->setFlash('Erreur BDD');
//                }
//
//                header('Location: '.\Lib\Application::REP_RACINE.'admin?methode=ajout');
//                    exit();
            } else {

                $this->setFlash(implode('<br>', $produit->getErreur()));
            }
        }


        $this->render('admin/categorie/ajout.html.php');
        //var_dump($image);
    }

    public function updateAction() {
        echo 'update';
        $categorieManager = new \modele\CategorieManager;
        $categorie = $categorieManager->getCategorieById($_GET['id']);
        //var_dump($article);
//        $this->render('admin/article/update.html.php', ['article' => $article]); //methode  = article
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $categorie = new Categorie($_POST); //objet article dans lequel on mets les données POST
            //var_dump($_FILES);
            if (($categorie->getErreur() == [])) {


                $categorie->setSlug(Categorie::slugify($categorie->getTitre()));

                $categorieManager = new \modele\CategorieManager;

                if ($categorieManager->updateCategorie($categorie)) {
                    $this->setFlash('<div class="alert alert-success">Categorie mise à jour</div>');
                } else {
                    $this->setFlash('<div class="alert alert-danger" style="background-color: pink;">Erreur BDD</div>');
                }
                //header('Location: ' . \Lib\Application::REP_RACINE . 'admin');
                //exit();
//                //mode pour rediriger vers l'article enregistrer
////                if ($id = $am->ajoutArticle($article)) {
////                    $this->setFlash('Article enregistré');
////                    $article -> setId($id);
////                } else {
////                    $this->setFlash('Erreur BDD');
////                }
////
////                header('Location: '.\Lib\Application::REP_RACINE.'admin?methode=ajout');
////                    exit();
            } else {

                $this->setFlash(implode('<br>', $article->getErreur()));
            }
        }
//
        $this->render('admin/categorie/update.html.php', ['categorie' => $categorie]); //2eme argument doit etre un tableau (voir methode render dans backcontroleur ) on peut rajouter d'autre valeur dans ce tableau
//        $this->render('admin/article/update.html.php');
    }

    public function deleteAction() {

        $produitManager = new \modele\ProduitManager;
        $produit = $produitManager->getProduitById(0);
        //var_dump($article);
        $produitManager->deleteProduit($produit);


        header('Location: ' . \Lib\Application::REP_RACINE . 'admin?module=produit&methode=index');
        exit();
    }

}
