<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Controleur\Backend;

use Lib\backControleur;
use modele\Article;
use modele\ArticleManager;

/**
 * Description of ConnexionControleur
 *
 * @author Human Booster
 */
class ArticleControleur extends backControleur {

    public function indexAction() {//module = index
        echo 'je suis le ArticleControleur';
        if (isset($_GET['page'])) {
            $page = $_GET['page'];
        } else {
            $page = 1;
        }
        $limit = 5;
        $offset = ($page - 1) * $limit;
        $article = new Article; // clic droit fix uses pour utiliser les use pour éviter les backslash
        $articleManager = new ArticleManager;
        $articles = $articleManager->getAllArticles($offset, $limit);
        $total = $articleManager->getTotalArticles();

        $pages = ceil($total / $limit);

        $this->render('admin/article/index.html.php', ['all_articles' => $articles, 'page' => $page, 'pages' => $pages]); //methode  = article
        //var_dump($users);
    }

    public function ajoutAction() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $article = new Article($_POST); //objet article dans lequel on mets les données POST
            //var_dump($_FILES);

            if ($article->getErreur() == []) {
                if (($_FILES['image']['error']) != 4) {
                    $upload = $article->upload(__DIR__ . '/../../Web/images/upload', $_FILES['image']);
                    //var_dump($upload);
                    if ($upload['upload']) {
                        $mini = $article->minImg(__DIR__ . '/../../Web/images/upload/' . $upload['message'], __DIR__ . '/../../Web/images/thumbnails');
                        $article->setImage($mini);
                        $article->setImage_originale($upload['message']);
                    } else {
                        $this->setFlash($upload['message']);
                    }
                }
                $article->setSlug(Article::slugify($article->getTitre()));
                $article->setAuteur($this->app->getUser());
                $am = new ArticleManager();
                if ($am->ajoutArticle($article)) {
                    $this->setFlash('<div class="alert alert-success">Article enregistré</div>');
                } else {
                    $this->setFlash('<div class="alert alert-danger" style="background-color: pink;">Erreur BDD</div>');
                }
                //header('Location: ' . \Lib\Application::REP_RACINE . 'admin');
                //exit();
                //mode pour rediriger vers l'article enregistrer
//                if ($id = $am->ajoutArticle($article)) {
//                    $this->setFlash('Article enregistré');
//                    $article -> setId($id);
//                } else {
//                    $this->setFlash('Erreur BDD');
//                }
//
//                header('Location: '.\Lib\Application::REP_RACINE.'admin?methode=ajout');
//                    exit();
            } else {

                $this->setFlash(implode('<br>', $article->getErreur()));
            }
        }


        $this->render('admin/article/ajout.html.php');
    }

    public function updateAction() {
        echo 'update';
        $articleManager = new \modele\ArticleManager;
        $article = $articleManager->getArticleById($_GET['id']);
        //var_dump($article);
//        $this->render('admin/article/update.html.php', ['article' => $article]); //methode  = article
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $article = new Article($_POST); //objet article dans lequel on mets les données POST
            //var_dump($_FILES);
            if (($article->getErreur() == [])) {
                if (($_FILES['image']['error']) != 4) {

                    $upload = $article->upload(__DIR__ . '/../../Web/images/upload', $_FILES['image']);
                    //var_dump($upload);
                    if ($upload['upload']) {
                        $mini = $article->minImg(__DIR__ . '/../../Web/images/upload/' . $upload['message'], __DIR__ . '/../../Web/images/thumbnails');
                        $article->setImage($mini);
                        $article->setImage_originale($upload['message']);
                    } else {
                        $this->setFlash($upload['message']);
                    }
                }

                $article->setSlug(Article::slugify($article->getTitre()));
                $article->setAuteur($this->app->getUser());
                $article->setPublier($article->getPublier());
                $articleManager = new \modele\ArticleManager;

                if ($articleManager->updateArticle($article)) {
                    $this->setFlash('<div class="alert alert-success">Article mis à jour</div>');
                } else {
                    $this->setFlash('<div class="alert alert-danger" style="background-color: pink;">Erreur BDD</div>');
                }
                //header('Location: ' . \Lib\Application::REP_RACINE . 'admin');
                //exit();
//                //mode pour rediriger vers l'article enregistrer
////                if ($id = $am->ajoutArticle($article)) {
////                    $this->setFlash('Article enregistré');
////                    $article -> setId($id);
////                } else {
////                    $this->setFlash('Erreur BDD');
////                }
////
////                header('Location: '.\Lib\Application::REP_RACINE.'admin?methode=ajout');
////                    exit();
            } else {

                $this->setFlash(implode('<br>', $article->getErreur()));
            }
        }
//
        $this->render('admin/article/update.html.php', ['article' => $article]); //2eme argument doit etre un tableau (voir methode render dans backcontroleur ) on peut rajouter d'autre valeur dans ce tableau
//        $this->render('admin/article/update.html.php');
    }

    public function deleteAction() {
        echo 'delete';


        $articleManager = new \modele\ArticleManager;
        $article = $articleManager->getArticleById($_GET['id']);
        //var_dump($article);
        $articleManager->deleteArticle($article);


        header('Location: ' . \Lib\Application::REP_RACINE . 'admin?methode=index');
        exit();
//                } else {
//                    $this->setFlash('Erreur BDD');
//                }
////                //mode pour rediriger vers l'article enregistrer
//////                if ($id = $am->ajoutArticle($article)) {
//////                    $this->setFlash('Article enregistré');
//////                    $article -> setId($id);
//////                } else {
//////                    $this->setFlash('Erreur BDD');
//////                }
//////
//////                header('Location: '.\Lib\Application::REP_RACINE.'admin?methode=ajout');
//////                    exit();
    }

}
