<?php

use Lib\Backend;

include '../Lib/Autoload.php';

session_start(); // ouvrir session après autoload pour récuperer les objets
$app = new Backend();
$app->run();
