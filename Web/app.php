<?php

use Lib\Frontend;

include '../Lib/Autoload.php';

session_start(); // ouvrir session après autoload pour récuperer les objets
$app = new Frontend();
$app->run();
