<?php

if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLHttpRequest') {
    exit;
}
sleep(1);

//echo json_encode($_GET); //pour tester la requete, on peut tester en direct avec methode GET en tapant dans l'url : http://localhost/phphb/AJAX/Ajax_login/ajax.listes.requete.php?id=1
//
//var_dump($_GET);



$host = 'localhost';
$dbname = 'exoobjetpoo';
$login = 'root';
$passwordBDD = '';

try {

    function upload($dir, $file) {
        if (!is_dir($dir)) {
            return ['upload' => false, 'message' => 'Ce dossier n\'existe pas'];
        }
        if (($file['error']) != 0) {
            switch ($file['error']) {
                case UPLOAD_ERR_INI_SIZE://case 1
                    $message = "Erreur : " . UPLOAD_ERR_INI_SIZE . " - La taille du fichier téléchargé excède la valeur de upload_max_filesize (2M), configurée dans le php.ini";
                    break;
                case UPLOAD_ERR_FORM_SIZE://case 2
                    $message = "Erreur : " . UPLOAD_ERR_FORM_SIZE . " - La taille du fichier téléchargé excède la valeur de MAX_FILE_SIZE, qui a été spécifiée dans le formulaire HTML.";
                    break;
                case UPLOAD_ERR_PARTIAL://case 3
                    $message = "Erreur : " . UPLOAD_ERR_PARTIAL . " -  Le fichier n'a été que partiellement téléchargé.";
                    break;
                case UPLOAD_ERR_NO_FILE://case 4
                    $message = "Erreur : " . UPLOAD_ERR_NO_FILE . " -  Aucun fichier n'a été téléchargé.";
                    break;
                default :
                    $message = "ERREUR UPLOAD";
            }
            return ['upload' => false, 'message' => $message];
        }
        $extension = pathinfo($file['image'], PATHINFO_EXTENSION);
        $extension = strtolower($extension); //mettre en minuscule
        //$extension_valide = ['jpg', 'jpeg', 'gif', 'png']; // méthode pas assez sécurisée car ne détecte que l'extension et pas le contenu du fichier

        $typesMime = ['image/jpg', 'image/jpeg', 'image/gif', 'image/png'];

        $finfo = new \finfo(FILEINFO_MIME_TYPE); //donner argument type mime au constructeur  de la classe finfo (class native)
        $type = $finfo->file($file['tmp_name']);
        var_dump($type);
        if (!in_array($type, $typesMime)) {

            return ['upload' => false, 'message' => 'CHOISIR UNE IMAGE AU BON FORMAT'];
        }
        $fileName = sha1(uniqid(rand(), true)) . '.' . $extension;
        $destination = $dir . '/' . $fileName;
        var_dump($destination);
        if (move_uploaded_file($file['tmp_name'], $destination)) {
            return ['upload' => true, 'message' => $fileName];
        } else {
            return ['upload' => false, 'message' => 'Erreur copie fichier'];
        }
    }

    if (($_FILES['image']['error']) != 4) {
        upload(__DIR__ . '/../../Web/images', $_FILES['image']);
    }


    $db = new PDO('mysql:host=' . $host . ';dbname=' . $dbname, $login, $passwordBDD, [
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES UTF8', //déclaré jeux de carctere pour conserver les accents
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, //afficher les erreur sql
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ //déclaré le mode de fetch par défaut
            ]
    );
    //var_dump($_GET['image']);
    $result = $db->prepare("INSERT INTO image (id,url,alt) VALUES (NULL,:url,:alt);");
    $result->bindValue(':url', $_GET['image'], PDO::PARAM_STR);
    $result->bindValue(':alt', $_GET['image'], PDO::PARAM_STR);
//    $result->execute();
//    $donnee = $result->fetchAll();
    //var_dump($donnee);

    if ($result->execute()) {

        $test = true;
    } else {
        $test = FALSE;
//        $donnee = 'Pas de r&#233;sultat';
    }
} catch (PDOException $exc) {
//    $donnee = $exc->getMessage();
    $test = FALSE;
}
exit(json_encode(['test' => $test]));
