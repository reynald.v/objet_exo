function extractFilename(path) {
    if (path.substr(0, 12) == "C:\\fakepath\\")
        return path.substr(12); // modern browser
    var x;
    x = path.lastIndexOf('/');
    if (x >= 0) // Unix-based path
        return path.substr(x + 1);
    x = path.lastIndexOf('\\');
    if (x >= 0) // Windows-based path
        return path.substr(x + 1);
    return path; // just the file name
}

$(function () {
    $('#image').change(function () {
        console.log($(this).val());
        if (($(this).val()) != 0) {// != pour tester seulement la valeur et non le type avec !==
            $('#divresult').html('<img src="images/loading_spinner.gif" />');
            $.ajax({
                method: "GET", //methode par defaut donc ici facultatif
                url: "ajax.listes.requete.php", //fichier php qui va contenir les requetes
                data: //données que l'on va envoyer
                        {
                            image: extractFilename($(this).val())
                        },
                dataType: "json" //format des données que l'on va récuperer
            }).done(function (data_json) {//méthode supplémentaire pour la fct ajax

                console.log(data_json);
                if (data_json.test) {
                    $('#divresult').css('backgroundColor', 'green').slideDown().html('OK');

//                } else {
//                    $('#divresult').html(data_json.liste);
                }
            }).fail(function () {
                $('#divresult').css('backgroundColor', 'red').slideDown().html('ECHEC AJAX');

            })
        } else {
            $('#divresult').empty();

        }
    });
});