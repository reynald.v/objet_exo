<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Frontend
 *
 * @author Human Booster
 */

namespace Lib;

class Frontend extends Application {

    public function __construct() {
        $this->layout = 'layout.html.php';
        $this->name = 'Frontend';
        parent::__construct();
    }

    public function run() {
        //echo 'appli lançée';
        //appeler les classes
        if (isset($_GET['module'])):
            $module = $_GET['module'];
        else:
            $module = 'catalogue';
        endif;
        //appeler les méthodes
        if (isset($_GET['methode'])):
            $action = $_GET['methode'];
        else:
            $action = 'allproduit';
        endif;

//methode automatisée
//            $nomControleur = '\Controleur\\' . ucfirst($module) . 'Controleur';
//            $controleur = new $nomControleur();
//            $controleur->indexAction();
//methode non automatisée
//            $controleur = new \Controleur\blogControleur();
//            $controleur->indexAction();

        $controleur = $this->getControleur($module);
        //$controleur->indexAction();
//            //methode non automatisée
//            $methode = $action . 'Action';
//            if (method_exists($controleur, $methode)) {
//                $controleur->$methode();
//            } else {
//                throw new Exception('La méthode n\existe pas');
//            }


        $controleur->getAction($action);
    }

}
