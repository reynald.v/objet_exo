<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lib;

use PDO;

/**
 * Description of PdoFactory
 *
 * @author Human Booster
 */
abstract class PdoFactory {//Design Pattern = Fabrique des instance de PDO

    public static $pdo = null;

    const HOST = 'localhost';
    const BDD = 'exoobjetpoo';
    const USER = 'root';
    const PWD = '';

//Design Pattern = Singleton : création d'une instance unique (ici on a une variable static null au départ, si elle est nulle on la créé sinon on la retourne directement
    static public function get() {//dans une méthode static on ne peut pas utiliser $this mais self:: quand c'est une variable ou un objet à l'interieur de la classe
//        if (SELF::$pdo == NULL) {
//            SELF::$pdo = new PDO('mysql:host=' . self::HOST . ';dbname=' . self::BDD, self::USER, self::PWD, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
//            return SELF::$pdo;
//        } else {
//            return SELF::$pdo;
//        }
        //équivaut à
        if (SELF::$pdo === NULL) {
            SELF::$pdo = new PDO('mysql:host=' . self::HOST . ';dbname=' . self::BDD, self::USER, self::PWD, [PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
        }
        return SELF::$pdo;
    }

}
