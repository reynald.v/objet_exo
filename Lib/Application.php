<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Application
 *
 * @author Human Booster
 */

namespace Lib;

use Exception;

abstract class Application {//car son but est d'etre héritée et pas instancier

    protected $user, $layout, $name; //variable pour gerer entre front end et back end

    public function __construct() {
        //initialisation d'un user, soit connecté , soit vide (dont les propriétés sont vides)
        if (isset($_SESSION['user'])) {

            $this->user = $_SESSION['user'];
        } else {
            //on créé ici un nouvel utilisateur de session
            $this->user = new \modele\User();
        }
    }

    public abstract function run(); //methode abstraite pour obliger toutes les filles de cette classe à devoir la réécrire

    const REP_IMAGES = '/OBJET_EXO/Web/images/';
    const REP_RACINE = '/OBJET_EXO/'; //on fait une variable du chemin à la racine pour les rewriting d'url

    protected function getControleur($module) {
        $control = '\Controleur\\' . $this->name . '\\' . ucfirst($module) . 'Controleur';
        //$controleur = new $nomControleur();
        if (class_exists($control)) {
            return new $control($this); //ici on fait passer backend ou frontend
        } else {
            //throw new Exception('Module non trouvé');
            throw new \Exception('Module non trouvé', 404);
        }
    }

    public function getUser() {
        return $this->user;
    }

    public function getLayout() {
        return $this->layout;
    }

    public function setUser($user) {
        $this->user = $user;
        return $this;
    }

    public function setLayout($layout) {
        $this->layout = $layout;
        return $this;
    }

}
