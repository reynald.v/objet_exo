<?php

function autoload($classe) {
    $classe = str_replace("\\", "/", $classe);
    $file = '../' . $classe . '.php';
    //echo $file;
    if (file_exists($file)) {
        include $file;
    }
}

spl_autoload_register('autoload');
