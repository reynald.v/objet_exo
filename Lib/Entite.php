<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lib;

/**
 * Description of Entite
 *
 * @author Human Booster
 */
abstract class Entite {

    use \Tools\Extrait;
    use \Tools\FormatDate;
    use \Tools\upLoadImage;

    protected $id;
    protected $erreur = [];

    public function __construct($data = []) {
        $this->hydratation($data);
    }

    protected function hydratation($data = []) {
        foreach ($data as $key => $value) {
            $setter = 'set' . ucfirst($key);
            if (method_exists($this, $setter)) {
                $this->$setter($value);
            }
        }
    }

    static public function slugify($titre) {
        // replace non letter or digits by -
        $titre = preg_replace('~[^\pL\d]+~u', '-', $titre);

        // transliterate
        $titre = iconv('utf-8', 'us-ascii//TRANSLIT', $titre);

        // remove unwanted characters
        $titre = preg_replace('~[^-\w]+~', '', $titre);

        // trim
        $titre = trim($titre, '-');

        // remove duplicate -
        $titre = preg_replace('~-+~', '-', $titre);

        // lowercase
        $titre = strtolower($titre);

        if (empty($titre)) {
            return 'n-a';
        }

        return $titre;
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
        return $this;
    }

    public function getErreur() {
        return $this->erreur;
    }

    public function setErreur($erreur) {
        $this->erreur[] = $erreur;
        return $this;
    }

}
