<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Frontend
 *
 * @author Human Booster
 */

namespace Lib;

class Backend extends Application {

    public function __construct() {
        $this->layout = 'layout_admin.html.php';
        $this->name = 'Backend';
        parent::__construct();
    }

    public function run() {//comme Backend extends Application elle doit surcharger la méthode RUN
        // var_dump($this->user);
        if ($this->user->isAdmin()) {//ici user est une propriété du backend donc $this
            if (($_SESSION['IPaddress'] != sha1($_SERVER['REMOTE_ADDR'])) || $_SESSION['userAgent'] != sha1($_SERVER['HTTP_USER_AGENT'])) {
                exit('GRRR'); //on verifie que les variables stockées sont les mêmes que celles stockées en session dans connexionControleur
            }

            if (isset($_GET['module'])):
                $module = $_GET['module'];
            else:
                $module = 'produit';
            endif;
            //appeler les méthodes
            if (isset($_GET['methode'])):
                $action = $_GET['methode'];
            else:
                $action = 'index';
            endif;

            $controleur = $this->getControleur($module);

            $controleur->getAction($action);
        } else {//l'utilisateur n'est pas connecté :
            $connexion = new \Controleur\Backend\ConnexionControleur($this);
            //équivaut à : $controleur = $this->getControleur($module);
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $connexion->loginAction();
            } else {

                $connexion->indexAction();
            }
        }
    }

}
