<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Lib;

use Exception;

/**
 * Description of backControleur
 *
 * @author Human Booster
 */
abstract class backControleur {

    protected $app;

//propriété app défini contenant backend et frontend
    public function __construct($app) {
        $this->app = $app;
    }

    public function getAction($action) {//function automatisée pour recuperer l'action et déclencher la méthode appropriée dans le front end
        $methode = $action . 'Action';
        //if (is_callable($this, $methode)){
        if (method_exists($this, $methode)) {//on remplace $controleur par $this car on est dans le controleur
            $this->$methode();
        } else {
            throw new Exception('La méthode n\existe pas');
        }
    }

    abstract protected function indexAction(); //methode abstraite pour obliger toutes les filles de cette classe à devoir la réécrire _ permet d'afficher l'index par défaut (page par défaut)

    protected function render($vue, array $data = []) {//$articles de blog controleur est passé ici en 2eme argumennt et s'appelle ensuite $data dans la vue
        extract($data); //Importe les variables dans la table des symboles
        ob_start();

        include __DIR__ . '/../Vue/' . $vue;

        $contenu = ob_get_clean();
        //include __DIR__ . '/../Vue/layout.html.php';
        include __DIR__ . '/../Vue/' . $this->app->getLayout();
        //var_dump($data);
    }

    ////GESTION DU MESSAGE D'ERREUR

    protected function setFlash($messageFlash) {//enregister une variable de session contenant le message d'erreur
        $_SESSION["messageFlash"] = $messageFlash;
    }

    protected function hasFlash() {//hasFlash retourne un booleen , est ce que un message flash existe ? vrai ou faux
//        if (isset($_SESSION["messageFlash"])) {
//            return true;
//        } else {
//            return false;
//        }
        //équivaut à:
        return isset($_SESSION["messageFlash"]); //retourne vrai ou faux
    }

    protected function getFlash() {//getFlash supprime la variable de session message d'erreur et retourne sa valeur
        $messageFlash = $_SESSION["messageFlash"]; //on stocke la variable de session dans une variable intermédiaire
        unset($_SESSION["messageFlash"]);
        return $messageFlash;
    }

}
