<?php

namespace Tools;

trait FormatDate {

    public function getFormatDate($str) {
        setlocale(LC_ALL, '');
        echo utf8_encode(strftime("%A %d %B %Y", strtotime($str)));
    }

}
