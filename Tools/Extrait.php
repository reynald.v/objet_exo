<?php

namespace Tools;

trait Extrait {

    public function getExtrait($text, $max = 200) {
        $text = strip_tags($text);
        $len = strlen($text);

        switch ($len) {
            case ($len > $max) :
                $extrait = substr($text, 0, $max);
                $pos = strrpos($extrait, ' ');
                if ($pos != false) {
                    $extrait = substr($text, 0, $pos);
                }
                return $extrait . ' [...]';
                break;
            case ($len < $max):
                echo $text;
                break;
        }
    }

}
