<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

/**
 * Description of Produit
 *
 * @author Human Booster
 */
class Produit extends \Lib\Entite {

    protected $titre, $contenu, $prix, $slug, $publier;

    /**
     *
     * @var type
     */
    protected $auteur;

    /**
     *
     * @var \Datetime
     */
    protected $date;

    /**
     *
     * @var image
     */
    protected $image;

    /**
     *
     * @var Categorie
     */
    protected $categorie;

    public function __construct($data = []) {
        $this->date = new \DateTime();
        parent::__construct($data); //recupererle data du parent
    }

    public function getAuteur() {
        return $this->auteur;
    }

    public function getDate() {

        return $this->date;
    }

    public function getPublier() {
        return $this->publier;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function getContenu() {
        return $this->contenu;
    }

    public function getSlug() {
        return $this->slug;
    }

    public function getImage() {
        return $this->image;
    }

    public function getCategorie() {
        return $this->categorie;
    }

    public function setAuteur($auteur) {
        $this->auteur = $auteur;
        return $this;
    }

    public function setSlug($slug) {
        $this->slug = $slug;
        return $this;
    }

    public function setTitre($titre) {
        $this->titre = $titre;
        return $this;
    }

    public function setContenu($contenu) {
        $this->contenu = $contenu;
        return $this;
    }

    public function setImage($image) {
        $this->image = $image;
        return $this;
    }

    public function setCategorie(Categorie $categorie) {
        $this->categorie = $categorie;
        return $this;
    }

    public function setDate($date) {
        $this->date = new \DateTime($date); //on converti la date au format string en objet Datetime prédéfini PHP
        return $this;
    }

    public function setPublier($publier) {
        $this->publier = $publier;
        return $this;
    }

}
