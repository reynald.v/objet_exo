<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

/**
 * Description of categorie
 *
 * @author Human Booster
 */
class categorie extends \Lib\Entite {

    public function getTitre() {
        return $this->titre;
    }

    public function getSlug() {
        return $this->slug;
    }

    public function setTitre($titre) {
        $this->titre = $titre;
        return $this;
    }

    public function setSlug($slug) {
        $this->slug = $slug;
        return $this;
    }

    protected $titre;
    protected $slug;

}
