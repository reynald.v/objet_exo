<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

use Lib\EntiteManager;
use PDO;

/**
 * Description of ProduitManager
 *
 * @author Human Booster
 */
class ProduitManager extends EntiteManager {

    public function getProduitByCategorie($id) {

        $id = $_GET['id'];
        $result = $this->pdo->prepare('SELECT categorie.id, categorie.titre titreCat, produit.id, produit.date, produit.titre titreProd,produit.contenu,produit.image,produit.categorie,produit.slug,produit.publier FROM produit LEFT JOIN categorie ON categorie.id=produit.categorie WHERE categorie=:id AND publier = 1 ;');
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Produit::class);
        $produits = $result->fetchAll();

        $im = new ImageManager(); //creer image manager pour retourner un objet image dans le tableau produits
        foreach ($produits as $produit) {
            if ($produit->getImage() !== null) {
                $img = $im->getImageById($produit->getImage());
                $produit->setImage($img);
            }
        }
        foreach ($produits as $produit) {//pour obtenir la date en objet et non en string
            $produit->setDate($produit->getDate());
        }
        //var_dump($produits);
        return $produits;
    }

    public function getProduitById($id) {

        $id = $_GET['id'];
        $result = $this->pdo->prepare('SELECT * FROM produit LEFT JOIN user ON user.id=produit.auteur WHERE produit.id=:id;');
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Produit::class);
        $produit = $result->fetch();
        //var_dump($produit);
        $im = new ImageManager(); //creer image manager pour retourner un objet image dans le tableau produits
        if ($produit->getImage() !== null) {
            $img = $im->getImageById($produit->getImage());
            $produit->setImage($img);
        }
        //var_dump($produit);
        return $produit;
    }

    public function getAllProduits($offset, $limit) {//méthode publique car on fait appelle à elle dans un controleur
        $sql = 'SELECT categorie.titre titreCat, produit.id, produit.date, produit.titre titreProd, produit.contenu, produit.image, produit.categorie, produit.publier, produit.slug, image.url,image.alt,image.miniature,login FROM produit LEFT JOIN image ON produit.image=image.id RIGHT JOIN categorie ON categorie.id=produit.categorie INNER JOIN user ON user.id=produit.auteur ORDER BY produit.id DESC LIMIT :offset,:limit';

        $result = $this->pdo->prepare($sql); //on fait juste une query puisque l'on a pas d'injection
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Produit::class); //recuperer un objet article (tableau associatif: fetch assoc)
        $produits = $result->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Produit::class);
        foreach ($produits as $produit) {//pour obtenir la date en objet et non en string
            $produit->setDate($produit->getDate());
        }
        //var_dump($produits);
        return $produits;
    }

    public function getTotalProduits() {

        $sql = 'SELECT count(*) total from produit';
        $result = $this->pdo->query($sql);
        $total = $result->fetchColumn();
        return $total;
    }

    public function ajoutProduit($produit) {

//        $result = $this->pdo->prepare('SELECT categorie.id catId,categorie.titre catTitre FROM categorie');
//        $result->execute();
//        $donneeAllCat = $result->fetchAll();
        //$idCategorie = $_POST['choixCat'];
        $categorieId = $_POST['categorieId'];
        $publier = $_POST['publier'];
//        $result = $this->pdo->prepare("INSERT INTO image (id,url,alt) VALUES (NULL,:url,:alt);");
//        $result->bindValue(':url', \modele\ImageManager::$image->getUrl(), PDO::PARAM_STR);
//        $result->bindValue(':alt', \modele\ImageManager::$image->getAlt(), PDO::PARAM_STR);
        //$result = $this->pdo->prepare('BEGIN TRANSACTION;');
        $result = $this->pdo->prepare('INSERT INTO produit (id,date,slug,titre,contenu,image,categorie,publier,auteur) VALUES (NULL,:date,:slug,:titre,:contenu,:image,:categorie,:publier,:auteur);');
        //$result = $this->pdo->prepare('INSERT INTO image (id,url,alt) VALUES (NULL,:url,:alt);');
        //$result = $this->pdo->prepare('COMMIT;');
        $result->bindValue(':date', $produit->getDate()->format('Y-m-d'), PDO::PARAM_STR);
        $result->bindValue(':titre', $produit->getTitre(), PDO::PARAM_STR);
        $result->bindValue(':contenu', $produit->getContenu(), PDO::PARAM_STR);
        $result->bindValue(':image', $produit->getImage(), PDO::PARAM_INT);
        $result->bindValue(':categorie', $categorieId, PDO::PARAM_INT);
        $result->bindValue(':publier', $publier, PDO::PARAM_BOOL);
        //$result->bindValue(':url', $produit->getImage($image->getUrl()), PDO::PARAM_STR);
        //$result->bindValue(':date', $produit->getDate()->format('Y-m-d'), PDO::PARAM_STR);
        $result->bindValue(':slug', $produit->getSlug(), PDO::PARAM_STR);
        $result->bindValue(':auteur', $produit->getAuteur()->getId(), PDO::PARAM_INT);
        //$result->bindValue(':url', $image->setUrl(), PDO::PARAM_STR);
        // $result->bindValue(':auteur', $produit->getAuteur()->getId(), PDO::PARAM_INT);
//        $result->bindValue(':url', $image->getUrl(), PDO::PARAM_STR);
//        $result->bindValue(':alt', $image->getAlt(), PDO::PARAM_STR);
        // var_dump($produit);
        try {


            $result->execute();

            return true; //return $this->pdo->lastInsertId();
        } catch (\PDOException $exc) {
            return false;
        }
    }

    public function selectImageByProduit($id) {
        //$id = $_GET['id'];

        $result = $this->pdo->prepare('SELECT image.id FROM image LEFT JOIN produit ON image.id=produit.image WHERE produit.id=' . $id . ';');
        $result->execute();
        $image = $result->fetch();
//        $im = new ImageManager;
//        $imageNew = $im->getLastIdImage();
        //var_dump($image);
        return $image;
    }

    public function updateProduit($produit) {
        $id = $_GET['id'];
        $categorieId = $_POST['categorieId'];
        if ($produit->getPublier()) {
            $produit->setPublier(0);
        } else {
            $produit->setPublier(1);
        }
//        $result = $this->pdo->prepare('SELECT categorie.id catId,categorie.titre catTitre FROM categorie');
//        $result->execute();
//        $donneeAllCat = $result->fetchAll();
        //$idCategorie = $_POST['choixCat'];
//        $categorieId = $_POST['categorieId'];
        $sql = 'UPDATE produit SET produit.id=:id,date=NOW(),slug=:slug,titre=:titre,contenu=:contenu,categorie=:categorie,publier=:publier,auteur=:auteur';
        if ($produit->getImage() !== null) {
            $sql .= ', image=:image WHERE produit.id=:id;';
            $result = $this->pdo->prepare($sql);
            $result->bindValue(':image', $produit->getImage(), PDO::PARAM_INT);
//            $result->bindValue(':image_originale', $article->getImage_originale(), PDO::PARAM_STR);
        } else {
            $sql .= ' WHERE produit.id=:id;';
            $result = $this->pdo->prepare($sql);
        }


        $result->bindValue(':id', $id, PDO::PARAM_INT);
        $result->bindValue(':titre', $produit->getTitre(), PDO::PARAM_STR);
        $result->bindValue(':contenu', $produit->getContenu(), PDO::PARAM_STR);
        $result->bindValue(':publier', $produit->getPublier(), PDO::PARAM_BOOL);

        $result->bindValue(':categorie', $categorieId, PDO::PARAM_INT);
        $result->bindValue(':slug', $produit->getSlug(), PDO::PARAM_STR);
        $result->bindValue(':auteur', $produit->getAuteur()->getId(), PDO::PARAM_INT);
        try {
            $result->execute();

            return true; //return $this->pdo->lastInsertId();
        } catch (\PDOException $exc) {
            return false;
        }
    }

    public function deleteProduit($produit) {

        $id = $_GET['id'];
        //var_dump($id);
        $result = $this->pdo->prepare('DELETE FROM `produit` WHERE `id` =:id;');
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }

}
