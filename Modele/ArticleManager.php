<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

use Lib\EntiteManager;
use PDO;

/**
 * Description of ArticleManager
 *
 * @author Human Booster
 */
class ArticleManager extends EntiteManager {

    public function get3LastArticle() {//méthode publique car on fait appelle à elle dans un controleur
        echo 'coucou';
        $sql = 'SELECT article.id,slug,titre,contenu,date,login,image,image_originale,publier FROM article LEFT OUTER JOIN user ON user.id=article.auteur WHERE publier = 1 ORDER BY date DESC LIMIT 10;';
        $result = $this->pdo->query($sql); //on fait juste une query puisque l'on a pas d'injection
        //$result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Article::class); //pour recuperer des objets de la classe Article
        //PDO::FETCH_PROPS_LATE = dans un premier temps association avec les attributs et 2eme temps fait passer le constructeur, pour inverser cet ordre , on utilise le drapeau
        $articles = $result->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Article::class);
        foreach ($articles as $article) {//pour obtenir la date en objet et non en string
            $article->setDate($article->getDate());
        }

        return $articles;
    }

    public function getArticleById() {

        $id = $_GET['id'];
        //var_dump($id);
        $result = $this->pdo->prepare('SELECT article.id,slug,titre,contenu,date,login,image,image_originale,publier FROM article LEFT JOIN user ON user.id=article.auteur WHERE article.id=:id;');
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Article::class);
        $article = $result->fetch();
        //var_dump($article);
        $article->setDate($article->getDate()); //sinon Date est une string
        return $article;
    }

    public function getAllArticles($offset, $limit) {//méthode publique car on fait appelle à elle dans un controleur
        $sql = 'SELECT article.id,slug,titre,contenu,date,login,image,publier FROM article LEFT JOIN user ON user.id=article.auteur ORDER BY date DESC LIMIT :offset,:limit;';
        $result = $this->pdo->prepare($sql); //on fait juste une query puisque l'on a pas d'injection
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Article::class); //recuperer un objet article (tableau associatif: fetch assoc)
        $articles = $result->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Article::class);
        foreach ($articles as $article) {//pour obtenir la date en objet et non en string
            $article->setDate($article->getDate());
        }
        //var_dump($articles);
        return $articles;
    }

    public function getTotalArticles() {

        $sql = 'SELECT count(*) total from article';
        $result = $this->pdo->query($sql);
        $total = $result->fetchColumn();
        return $total;
    }

    public function ajoutArticle($article) {
        //var_dump($article);
//        $auteur = $article->getAuteur()->getId();
//        $titre = $article->getTitre();
//        $contenu = $article->getContenu();
//        $slug = $article->getSlug();
//        $date = $article->getDate()->format('Y-m-d');
//        $image = $article->getImage();
//        $result = $this->pdo->prepare('INSERT INTO article (id,slug,titre,contenu,date,auteur,image) VALUES (NULL,:slug,:titre,:contenu,:date,:auteur,:image);');
//        $result->bindParam(':titre', $titre, PDO::PARAM_STR, 12);
//        $result->bindParam(':contenu', $contenu, PDO::PARAM_STR, 12);
//        $result->bindParam(':image', $image, PDO::PARAM_STR, 12);
//        $result->bindParam(':date', $date, PDO::PARAM_STR, 12);
//        $result->bindParam(':slug', $slug, PDO::PARAM_STR, 12);
//        $result->bindParam(':auteur', $auteur, PDO::PARAM_INT, 12);
//        $result->execute();
        //
//        if ($article->getPublier() !== null) {
//                $publier = 0;
//            } else {
//                $publier = 1;
//            }
        $result = $this->pdo->prepare('INSERT INTO article (id,slug,titre,contenu,date,auteur,image,image_originale,publier) VALUES (NULL,:slug,:titre,:contenu,:date,:auteur,:image,:image_originale,:publier);');
        $result->bindValue(':titre', $article->getTitre(), PDO::PARAM_STR);
        $result->bindValue(':contenu', $article->getContenu(), PDO::PARAM_STR);
        $result->bindValue(':image', $article->getImage(), PDO::PARAM_STR);
        $result->bindValue(':image_originale', $article->getImage_originale(), PDO::PARAM_STR);
        $result->bindValue(':publier', 1, PDO::PARAM_BOOL);
        $result->bindValue(':date', $article->getDate()->format('Y-m-d'), PDO::PARAM_STR);
        $result->bindValue(':slug', $article->getSlug(), PDO::PARAM_STR);
        $result->bindValue(':auteur', $article->getAuteur()->getId(), PDO::PARAM_INT);
        try {
            $result->execute();
            return true; //return $this->pdo->lastInsertId();
        } catch (\PDOException $exc) {
            return false;
        }
    }

    public function updateArticle($article) {
        //var_dump($article);
        $id = $_GET['id'];
        if ($article->getPublier()) {
            $article->setPublier(0);
        } else {
            $article->setPublier(1);
        }
        $sql = 'UPDATE article SET slug=:slug,titre=:titre,contenu=:contenu,date=NOW(),auteur=:auteur,publier=:publier';
        if ($article->getImage() !== null) {
            $sql .= ', image=:image, image_originale=:image_originale WHERE id=:id;';
            $result = $this->pdo->prepare($sql);
            $result->bindValue(':image', $article->getImage(), PDO::PARAM_STR);
            $result->bindValue(':image_originale', $article->getImage_originale(), PDO::PARAM_STR);
        } else {
            $sql .= ' WHERE id=:id;';
            $result = $this->pdo->prepare($sql);
        }

        $result->bindValue(':id', $id, PDO::PARAM_INT);
        $result->bindValue(':titre', $article->getTitre(), PDO::PARAM_STR);
        $result->bindValue(':contenu', $article->getContenu(), PDO::PARAM_STR);
        $result->bindValue(':publier', $article->getPublier(), PDO::PARAM_BOOL);
        $result->bindValue(':slug', $article->getSlug(), PDO::PARAM_STR);
        $result->bindValue(':auteur', $article->getAuteur()->getId(), PDO::PARAM_INT);
        try {
            $result->execute();

            return true; //return $this->pdo->lastInsertId();
        } catch (\PDOException $exc) {
            return false;
        }
    }

    public function deleteArticle($article) {

        $id = $_GET['id'];
        //var_dump($id);
        $result = $this->pdo->prepare('DELETE FROM `article` WHERE `id` =:id;');
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
    }

}
