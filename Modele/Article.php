<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

/**
 * Description of Article
 *
 * @author Human Booster
 */
class Article extends \Lib\Entite {

    protected $slug, $titre, $contenu, $image, $image_originale, $publier;

    /**
     *
     * @var type
     */
    protected $auteur;

    /**
     *
     * @var \Datetime
     */
    protected $date;

    /**
     *
     * @var Categorie
     */
    protected $categorie;

    public function __construct($data = []) {
        $this->date = new \DateTime();
        parent::__construct($data); //recupererle data du parent
    }

    public function getCategorie() {
        return $this->categorie;
    }

    public function getPublier() {
        return $this->publier;
    }

    public function setPublier($publier) {
        $this->publier = $publier;
        return $this;
    }

    public function getSlug() {
        return $this->slug;
    }

    public function getTitre() {
        return $this->titre;
    }

    public function getContenu() {
        return $this->contenu;
    }

    public function getImage() {
        return $this->image;
    }

    public function getImage_originale() {
        return $this->image_originale;
    }

    public function getAuteur() {
        return $this->auteur;
    }

    public function getDate() {

        return $this->date;
    }

    public function setCategorie(Categorie $categorie) {
        $this->categorie = $categorie;
        return $this;
    }

    public function setSlug($slug) {
        $this->slug = $slug;
        return $this;
    }

    public function setTitre($titre) {
        if (strlen($titre) < 2) {
            $this->erreur[] = "Le titre doit contenir plus de 2 caractères";
        } else {
            $this->titre = $titre;
        }

        return $this;
    }

    public function setContenu($contenu) {
        if ($contenu !== '') {
            $this->contenu = $contenu;
        } else {
            $this->erreur[] = "Ecrire un message SVP";
        }

        return $this;
    }

    public function setImage($image) {
        $this->image = $image;
        return $this;
    }

    public function setImage_originale($image_originale) {
        $this->image_originale = $image_originale;
        return $this;
    }

    public function setAuteur($auteur) {
        $this->auteur = $auteur;
        return $this;
    }

    public function setDate($date) {
        $this->date = new \DateTime($date); //on converti la date au format string en objet Datetime prédéfini PHP
        return $this;
    }

}
