<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

use Exception;

/**
 * Description of User
 *
 * @author Human Booster
 */
class User extends \Lib\Entite {

    protected $login, $password, $email, $privilege;

    public function getLogin() {
        return $this->login;
    }

    public function getPassword() {
        return $this->password;
    }

    public function getMail() {
        return $this->mail;
    }

    public function getPrivilege() {
        return $this->privilege;
    }

    public function setAuthentifie($authentification) {//si c'est vrai il s'est connecté, si false pas connecté
        if (!is_bool($authentification)) {
            throw new Exception('Un boolean svp');
        } else {
            $_SESSION['authentification'] = $authentification;
        }
    }

    public function isAuthentifie() {
        if (isset($_SESSION['authentification']) && $_SESSION['authentification'] === true) {
            return true;
        } else {
            return false;
        }
        //équivaut à : return isset($_SESSION['authentification']) && $_SESSION['authentification'] === true;
    }

    public function isAdmin() {// si c'est vrai ça retourne vrai, si c'est faux ça retourne faux
        return $this->isAuthentifie() && $this->getPrivilege() >= 2;
        //équivaut à : return $this->isAuthentifie() && $this->privilege >=2; car on est dans la classe User
    }

    public function isUser() {// si c'est vrai ça retourne vrai, si c'est faux ça retourne faux
        return $this->isAuthentifie() && $this->getPrivilege() < 2;
    }

    public function __sleep() {
        return array('id', 'login', 'privilege', 'email');
        //équivaut à : return ['id','login','privilege','email'];
    }

    public function setLogin($login) {
        $this->login = $login;
        return $this;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    public function setMail($mail) {
        $this->mail = $mail;
        return $this;
    }

    public function setPrivilege($privilege) {
        $this->privilege = $privilege;
        return $this;
    }

}
