<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

use Lib\EntiteManager;
use PDO;

/**
 * Description of CategorieManager
 *
 * @author Human Booster
 */
class CategorieManager extends EntiteManager {

    public function getAllCategories() {
        $sql = 'SELECT * FROM categorie ORDER BY titre ASC';
        $result = $this->pdo->query($sql);
        $categories = $result->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, categorie::class);
        return $categories;
    }

    public function getAllCategoriesWithProduit() {


//        $result = $this->pdo->prepare('SELECT categorie.id, categorie.titre titreCat, produit.id, produit.titre titreProd,produit.contenu,produit.image,produit.categorie,produit.prix,produit.slug FROM produit LEFT JOIN categorie ON categorie.id=produit.categorie WHERE categorie=:id;');

        $sql = 'SELECT * FROM produit LEFT JOIN categorie ON categorie.id=produit.categorie WHERE produit.publier = 1 GROUP BY produit.categorie;';
        $result = $this->pdo->query($sql);
        $categories = $result->fetchAll(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, categorie::class);
        return $categories;
    }

    public function getCategorieById() {

        $id = $_GET['id'];
        //var_dump($id);
//        $result = $this->pdo->prepare('SELECT categorie.id,slug,titre FROM categorie LEFT JOIN user ON user.id=article.auteur WHERE article.id=:id;');
        $result = $this->pdo->prepare('SELECT categorie.id,slug,titre FROM categorie WHERE categorie.id=:id;');
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Categorie::class);
        $categorie = $result->fetch();
        //var_dump($categorie);
        return $categorie;
    }

    public function ajoutCategorie($categorie) {

        $result = $this->pdo->prepare('INSERT INTO categorie (id,slug,titre) VALUES (NULL,:slug,:titre);');
        $result->bindValue(':titre', $categorie->getTitre(), PDO::PARAM_STR);
        $result->bindValue(':slug', $categorie->getSlug(), PDO::PARAM_STR);
        try {
            $result->execute();
            return true; //return $this->pdo->lastInsertId();
        } catch (\PDOException $exc) {
            return false;
        }
    }

    public function updateCategorie($categorie) {
        $id = $_GET['id'];
        $result = $this->pdo->prepare('UPDATE categorie SET slug=:slug,titre=:titre WHERE id=:id;');
        $result->bindValue(':id', $id, PDO::PARAM_INT);
        $result->bindValue(':titre', $categorie->getTitre(), PDO::PARAM_STR);
        $result->bindValue(':slug', $categorie->getSlug(), PDO::PARAM_STR);

        try {
            $result->execute();

            return true; //return $this->pdo->lastInsertId();
        } catch (\PDOException $exc) {
            return false;
        }
    }

}
