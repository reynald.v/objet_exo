<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

use Lib\EntiteManager;
use PDO;

/**
 * Description of UserManager
 *
 * @author Human Booster
 */
class UserManager extends EntiteManager {

    public function getUserByLogin($login) {

        //$password = $_POST['pwd'];
        $result = $this->pdo->prepare("SELECT id,login,password,privilege, email FROM user WHERE login=:login");
        $result->bindParam(':login', $login);
        $result->execute();
        $result->setFetchMode(\PDO::FETCH_CLASS, \modele\User::class);

        $user = $result->fetch();
        //var_dump($user);
        return $user;
        //var_dump($donnee);
//        if ($user == false) {
//            echo 'Le login n\'existe pas';
//        } else {
//            echo 'Le login existe';
//        }
    }

}
