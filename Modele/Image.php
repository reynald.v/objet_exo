<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

/**
 * Description of image
 *
 * @author Human Booster
 */
class image extends \Lib\Entite {

    protected $url, $alt, $miniature;

    public function getMiniature() {
        return $this->miniature;
    }

    public function getUrl() {
        return $this->url;
    }

    public function getAlt() {
        return $this->alt;
    }

    public function setUrl($url) {
        $this->url = $url;
        return $this;
    }

    public function setAlt($alt) {
        $this->alt = $alt;
        return $this;
    }

    public function setMiniature($miniature) {
        $this->miniature = $miniature;
        return $this;
    }

}
