<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

/**
 * Description of Notes
 *
 * @author Human Booster
 */
class Notes extends \Lib\Entite {

    protected $auteur, $valeur, $date, $produit;

    public function __construct($data = array()) {
        $this->date = new \DateTime();
        parent::__construct($data);
    }

    public function getAuteur() {
        return $this->auteur;
    }

    public function getValeur() {
        return $this->valeur;
    }

    public function getDate() {
        return $this->date;
    }

    public function getProduit() {
        return $this->produit;
    }

    public function setAuteur($auteur) {
        //var_dump($auteur);
        if (strlen($auteur) > 2) {
            $this->auteur = $auteur;
        } else {
            $this->erreur[] = "Le nom doit contenir plus de 2 caractères";
        } return $this;
    }

    public function setValeur($valeur) {
        $this->valeur = $valeur;
        return $this;
    }

    public function setDate($date) {
        $this->date = new \DateTime($date); //on converti la date (au format string) en objet Datetime prédéfini PHP
        return $this;
    }

    public function setProduit($produit) {
        $this->produit = $produit;
        return $this;
    }

}
