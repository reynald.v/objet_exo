<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

use Lib\EntiteManager;
use PDO;

/**
 * Description of ImageManager
 *
 * @author Human Booster
 */
class ImageManager extends EntiteManager {

    public function getImageById($id) {

        //$id = $_GET['id'];
        $result = $this->pdo->prepare('SELECT * FROM image WHERE image.id=:id;');
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Image::class);
        $image = $result->fetch();


        //var_dump($image);

        return $image;
    }

    public function ajoutImageProd($image) {
        var_dump($image);

        //
        $result = $this->pdo->prepare('INSERT INTO image (id,url,alt,miniature) VALUES (NULL,:url,:alt,:miniature);');
//        $result->bindValue(':id', $image->getId(), PDO::PARAM_INT);
        $result->bindValue(':url', $image->getUrl(), PDO::PARAM_STR);
        $result->bindValue(':alt', $image->getAlt(), PDO::PARAM_STR);
        $result->bindValue(':miniature', $image->getMiniature(), PDO::PARAM_STR);
        try {
            $result->execute();

            return true;
            //return $this->pdo->lastInsertId();
        } catch (\PDOException $exc) {
            return false;
        }
    }

    public function updateImageProd($image) {

        //$id = getLastIdImage();
        //
        $result = $this->pdo->prepare('UPDATE image SET url=:url,alt=:alt,miniature=:miniature WHERE id=:id;');
        $result->bindValue(':id', $image->getId(), PDO::PARAM_INT);
        $result->bindValue(':url', $image->getUrl(), PDO::PARAM_STR);
        $result->bindValue(':alt', $image->getAlt(), PDO::PARAM_STR);
        $result->bindValue(':miniature', $image->getMiniature(), PDO::PARAM_STR);
        try {
            $result->execute();
            var_dump($image);
            return true;
            //return $this->pdo->lastInsertId();
        } catch (\PDOException $exc) {
            return false;
        }
    }

    public function getLastIdImage() {//méthode publique car on fait appelle à elle dans un controleur
        echo 'coucou';
        $sql = 'SELECT MAX(id) as "lastIdImage" FROM image';
        $result = $this->pdo->query($sql); //on fait juste une query puisque l'on a pas d'injection
        //$result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Article::class); //pour recuperer des objets de la classe Article
        //PDO::FETCH_PROPS_LATE = dans un premier temps association avec les attributs et 2eme temps fait passer le constructeur, pour inverser cet ordre , on utilise le drapeau
        $lastIdImage = $result->fetch();

        return $lastIdImage[0];
    }

}
