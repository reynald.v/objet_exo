<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace modele;

use Lib\EntiteManager;
use PDO;

/**
 * Description of ProduitManager
 *
 * @author Human Booster
 */
class NotesManager extends EntiteManager {

    public function getNotesByProduitId($id) {

        $id = $_GET['id'];
        $result = $this->pdo->prepare('SELECT * FROM notes WHERE produit=:id;');
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->execute();
        $result->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, Notes::class);
        $notes = $result->fetchAll();
        foreach ($notes as $note) {//pour obtenir la date en objet et non en string
            $note->setDate($note->getDate());
        }
        return $notes;
    }

    public function insertNote($notes) {

//ici on inserre un objet $notes dans la BDD et pas un tableau
        $id = $notes->getProduit();
        $auteur = $notes->getAuteur();
        $valeur = $notes->getValeur();

        $result = $this->pdo->prepare('INSERT INTO notes (id,auteur,valeur,date,produit) VALUES (NULL,:auteur,:valeur,NOW(),:id);');
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        $result->bindParam(':auteur', $auteur, PDO::PARAM_STR, 12);
        $result->bindParam(':valeur', $valeur, PDO::PARAM_INT);

        if ($result->execute()) {

            $success = "enregistrement réussi";
        } else {
            $success = "probleme d'envoi à la BDD";
        }
        return $success;
    }

}
