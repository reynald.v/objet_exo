<h1><?php echo $titre; ?> </h1>
<hr>
<?php foreach ($mes_articles as $donnee): ?>
    <div class="row">
        <article>

            <?php if ($donnee->getImage_originale() != null) { ?>
                <div class="col-xs-12 col-sm-6 col-md-4">
                    <img class="img-thumbnail" src="<?php echo Lib\Application::REP_IMAGES . "upload/" . $donnee->getImage_originale(); ?>" alt="<?php echo htmlentities($donnee->getTitre(), ENT_QUOTES); ?>">

                </div>
                <div class="col-xs-12 col-sm-6 col-md-8">
                <?php } else { ?>
                    <div class="col-xs-12 col-sm-6 col-md-12">

                    <?php } ?>


                    <a href="<?php echo Lib\Application::REP_RACINE ?>blog/detail/<?php echo $donnee->getSlug(); ?>-<?php echo $donnee->getId(); ?>"><h2 class="text-uppercase"><?php echo $donnee->getTitre(); ?></h2></a>

                    <p><?php echo $donnee->getExtrait($donnee->getContenu()); ?></p>
                    <h3>Auteur : <?php echo $donnee->login; ?></h3>
                    <h4>Date : <?php echo $donnee->getFormatDate($donnee->getDate()->format('d-m-Y'));
                    ?></h4>
                </div>
        </article>
    </div>

<?php endforeach; ?>