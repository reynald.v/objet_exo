<h1>Blog Detail</h1>
<hr>

<article>

    <h2 class="text-uppercase"><?php echo $article->getTitre(); ?></h2>

    <p><?php echo $article->getContenu(); ?></p>
    <h3>Auteur : <?php echo $article->login; ?></h3>
    <h4>Date : <?php echo $article->getFormatDate($article->getDate()->format('d-m-Y'));
?></h4>
</article>
<div>
    <?php if ($article->getImage_originale() != null): ?>
        <img class="img-thumbnail" src="<?php echo Lib\Application::REP_IMAGES . "upload/" . $article->getImage_originale(); ?>" alt="<?php echo htmlentities($article->getTitre(), ENT_QUOTES); ?>">
    <?php endif; ?>
</div>
<a href="<?php echo Lib\Application::REP_RACINE . 'blog' ?>"><h2 class="text-uppercase">Retour</h2></a>