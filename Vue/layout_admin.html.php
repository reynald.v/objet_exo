
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Catalogue formation human booster">


        <title>POO</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


        <!-- Custom styles for this template -->
        <link href="<?php echo Lib\Application::REP_RACINE ?>Web/css/main.css" rel="stylesheet">


    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">ADMIN POO Human Booster</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">

                        <li class="active"><a href="<?php echo Lib\Application::REP_RACINE . 'admin' ?>">Admin Articles</a></li>
                        <li><a href="<?php echo Lib\Application::REP_RACINE . 'appBackend.php?module=categorie&methode=index' ?>">Admin Categorie</a></li>

                        <li><a href="<?php echo Lib\Application::REP_RACINE ?>admin?module=connexion&methode=logOut">Déconnexion</a></li>
                        <li><a href="<?php echo Lib\Application::REP_RACINE . 'app.php' ?>" target="_blank">Site</a></li>
                    </ul>
                </div><!--/.nav-collapse -->


            </div>

        </nav>

        <div class="container">

            <div class="starter-template">
                <?php echo $contenu; ?>
            </div>

        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
<!--        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>-->
        <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>

<!--        <script src="<?php /* echo Lib\Application::REP_RACINE */ ?>Web/js/AjaxListe.jquery.js"></script>-->
    </body>
    <footer>
    </footer>
</html>
