<h1>DETAIL PRODUIT</h1>
<hr>
<?php
if ($this->hasFlash() == true) {//on peut mettre this car notre vue est incluse gràce à la fonction render dans produitAction du catalogue controleur
    ?><?php echo $this->getFlash(); ////on peut faire un echo de la fonction pour afficher le message vu que la fonction getFlash retourne le message      ?>
<?php } ?>
<div class="row">
    <article>
        <?php if ($produit->getImage() != null) { ?>
            <div class="col-xs-12 col-sm-6 col-md-6">
                <img class="img-thumbnail" src="<?php echo Lib\Application::REP_IMAGES . "upload/" . ($produit->getImage()->getUrl()); ?>" alt="<?php echo htmlentities($produit->getTitre(), ENT_QUOTES); ?>">
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6">
            <?php } else { ?>
                <div class="col-xs-12 col-sm-6 col-md-12">

                <?php } ?>
                <h2 class="text-uppercase"><?php echo $produit->getTitre(); ?></h2>

                <p><?php echo $produit->getContenu(); ?></p>
            </div>
    </article>

    <ul>
        <?php foreach ($notes as $note): ?>


            <li><?php echo $note->getValeur(); ?>-<?php echo $note->getAuteur(); ?>-<?php echo $note->getFormatDate($note->getDate()->format('d-m-Y')); ?></li>


        <?php endforeach; ?>
    </ul>
    <form method="post" action="<?php echo Lib\Application::REP_RACINE ?>catalogue/note/<?php echo $produit->getSlug(); ?>-<?php echo $produit->getId(); ?>">
        <input type="hidden" name="produit" value="<?php echo $produit->getId(); ?>" />
        <label>Nom <input type="text" name="auteur" /></label>
        <ul class="notes-echelle">
            <li>
                <label for="note01" title="Note&nbsp;: 1 sur 5">1</label>
                <input type="radio" name="valeur" id="note01" value="1" />
            </li>
            <li>
                <label for="note02" title="Note&nbsp;: 2 sur 5">2</label>
                <input type="radio" name="valeur" id="note02" value="2" />
            </li>
            <li>
                <label for="note03" title="Note&nbsp;: 3 sur 5">3</label>
                <input type="radio" name="valeur" id="note03" value="3" />
            </li>
            <li>
                <label for="note04" title="Note&nbsp;: 4 sur 5">4</label>
                <input type="radio" name="valeur" id="note04" value="4" />
            </li>
            <li>
                <label for="note05" title="Note&nbsp;: 5 sur 5">5</label>
                <input type="radio" name="valeur" id="note05" value="5" />
            </li>
        </ul>
        <button type="submit" name="ok" class="btn btn-default">ok</button>
    <!--        <select name="notes">
         <option>1</option>
         <option>2</option>
         <option>3</option>
         <option>4</option>
         <option>5</option>
     </select>-->
    </form>
</div>

