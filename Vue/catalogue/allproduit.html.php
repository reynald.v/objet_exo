
<h1><?php echo $titre; ?> </h1>
<hr>
<?php foreach ($all_produit as $donnee): ?>
    <?php if ($donnee->getPublier() != 0) { ?>
        <div class="row">
            <article>
                <?php if ($donnee->getImage() != null) { ?>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                        <img class="img-thumbnail" src="<?php echo Lib\Application::REP_IMAGES . "upload/" . ($donnee->url); ?>" alt="<?php echo htmlentities($donnee->titreProd, ENT_QUOTES); ?>">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6">
                    <?php } else { ?>
                        <div class="col-xs-12 col-sm-6 col-md-12">

                        <?php } ?>
                        <a href="<?php echo Lib\Application::REP_RACINE ?>catalogue/produit/<?php echo $donnee->getSlug(); ?>-<?php echo $donnee->getId(); ?>"><h2 class="text-uppercase"><?php echo $donnee->titreProd; ?></h2></a>


                        <p><?php echo $donnee->getContenu(); ?></p>
                        <p>Catégorie : <strong><?php echo $donnee->titreCat; ?></strong></p>
                        <p>Date : <strong><?php echo $donnee->getFormatDate($donnee->getDate()->format('d-m-Y'));
                        ?></strong></p>
                    </div>
            </article>
        </div>
    <?php } ?>
<?php endforeach; ?>



