<?php if ($produits != []) { ?>
    <h1>CATEGORIE : <?php echo $produits[0]->titreCat; ?></h1>
    <hr>
    <?php foreach ($produits as $donnee): ?>


        <div class="row">
            <article>
                <?php if ($donnee->getImage() != null) { ?>
                    <div class="col-xs-12 col-sm-6 col-md-4">
                        <img class="img-thumbnail" src="<?php echo Lib\Application::REP_IMAGES . "upload/" . ($donnee->getImage()->getUrl()); ?>" alt="<?php echo htmlentities($donnee->getTitre(), ENT_QUOTES); ?>"> </div>
                    <div class="col-xs-12 col-sm-6 col-md-8">
                    <?php } else { ?>
                        <div class="col-xs-12 col-sm-6 col-md-12">

                        <?php } ?>

                        <a href="<?php echo Lib\Application::REP_RACINE ?>catalogue/produit/<?php echo $donnee->getSlug(); ?>-<?php echo $donnee->getId(); ?>"><h2 class="text-uppercase"><?php echo $donnee->titreProd; ?></h2></a>

                        <p><?php echo $donnee->getExtrait($donnee->getContenu()); ?></p>
                        <p>Date : <?php echo $donnee->getFormatDate($donnee->getDate()->format('d-m-Y'));
                        ?></p>

                    </div>
            </article>
        </div>
    <?php endforeach; ?>
<?php } else { ?>
    <h1 class="text-uppercase">PAS DE PRODUIT DANS CETTE CATEGORIE</h1>
<?php } ?>
<a href="<?php echo Lib\Application::REP_RACINE . 'catalogue' ?>"><h2 class="text-uppercase">Retour</h2></a>