
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta name="description" content="Catalogue formation human booster">


        <title>POO</title>

        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">


        <!-- Custom styles for this template -->
        <link href="<?php echo Lib\Application::REP_RACINE ?>/Web/css/main.css" rel="stylesheet">


    </head>

    <body>

        <nav class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">POO Human Booster</a>
                </div>
                <div id="navbar" class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">

                        <li class="active"><a href="<?php echo Lib\Application::REP_RACINE . 'catalogue/allproduit' ?>">Tous les articles</a></li>
                        <li><div class="collapse navbar-collapse" id="oNavigation">
                                <ul class="nav navbar-nav">
                                    <li class="dropdown">
                                        <a href="<?php echo Lib\Application::REP_RACINE . 'catalogue' ?>" class="dropdown-toggle" data-toggle="dropdown">Catégories<b class="caret"></b> </a>
                                        <ul class="dropdown-menu">
                                            <?php include '/catalogue/index.html.php' ?>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div><!--/.nav-collapse -->

            </div>
        </nav>
        <div class="container">

            <div class="starter-template">
                <?php echo $contenu; ?>
            </div>

        </div><!-- /.container -->


        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    </body>
    <footer>
        <script type="text/javascript">
            $(document).ready(function () {

                $("ul.notes-echelle").addClass("js");
                $("ul.notes-echelle li").addClass("note-off");

                if ($("ul.notes-echelle li").hasClass('current'))
                {
                    $(this).prevAll("li").addClass("current");
                }

                $("ul.notes-echelle").focus(function () {
                    $(this).parent("li").addClass("note-focus");
                    $(this).parent("li").nextAll("li").addClass("note-off");
                    $(this).parent("li").prevAll("li").removeClass("note-off");
                    $(this).parent("li").removeClass("note-off");
                })

                $("ul.notes-echelle li").mouseover(function () {
                    $(this).nextAll("li").addClass("note-off");
                    $(this).prevAll("li").removeClass("note-off");
                    $(this).removeClass("note-off");
                });

                $("ul.notes-echelle").mouseout(function () {
                    $(this).children("li").addClass("note-off");
                });
                // On ajoute la classe "js" à la liste pour mettre en place par la suite du code CSS uniquement dans le cas où le Javascript est activé
                $("ul.notes-echelle").addClass("js");
                // On passe chaque note à l'état grisé par défaut
                $("ul.notes-echelle li").addClass("note-off");
                // Au survol de chaque note à la souris
                $("ul.notes-echelle li").mouseover(function () {
                    // On passe les notes supérieures à l'état inactif (par défaut)
                    $(this).nextAll("li").addClass("note-off");
                    // On passe les notes inférieures à l'état actif
                    $(this).prevAll("li").removeClass("note-off");
                    // On passe la note survolée à l'état actif (par défaut)
                    $(this).removeClass("note-off");
                });
                // Lorsque l'on sort du sytème de notation à la souris
                $("ul.notes-echelle").mouseout(function () {
                    // On passe toutes les notes à l'état inactif
                    $(this).children("li").addClass("note-off");
                    // On simule (trigger) un mouseover sur la note cochée s'il y a lieu
                    $(this).find("li input:checked").parent("li").trigger("mouseover");
                });

            });

        </script>
    </footer>
</html>
