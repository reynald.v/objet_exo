<?php
if ($this->hasFlash() == true) {//on peut mettre this car notre vue est incluse gràce à la fonction render dans produitAction du catalogue controleur
    ?><?php echo $this->getFlash(); ////on peut faire un echo de la fonction pour afficher le message vu que la fonction getFlash retourne le message      ?>
<?php } ?>
<h1>Update Produit</h1>


<form method="post" enctype="multipart/form-data">

    <fieldset>
        <legend>Mettre à jour l'articlet</legend>



        <hr>
        <div class="form-group">
            <label for="titre">Titre</label>
            <input class="form-control" id="titre" name="titre" type="text" value="<?php echo $produit->getTitre(); ?>">
            <?php if ($produit->getImage() != NULL) { ?>
                <input id="idImage" name="idImage" type="hidden" value="<?php echo $produit->getImage()->getId(); ?>">
            <?php } ?>
        </div>

        <div class="form-group">
            <label for="contenu">contenu</label>
            <textarea class="form-control ckeditor" id="contenu" name="contenu"><?php echo $produit->getContenu(); ?></textarea>
        </div>
        <br/>
        <label>Catégorie</label>
    </fieldset>

    <div class="form-group">
        <select class="form-control" name="categorieId">
            <?php foreach ($categories as $donnee) { ?>
                <?php /* if ($donnee->catId !== $donneeCat->catId) { */ ?>
                <option value="<?php echo $donnee->getId(); ?>"><?php echo $donnee->getTitre(); ?></option>
                <?php /* } else { */ ?>
    <!--                      <option selected="selected" value="<?php /* echo $donneeCat->catId; ?>"><?php echo $donneeCat->catNom; */ ?></option>-->
                <?php /* } */ ?>
            <?php } ?>
        </select>
    </div>

    <label>
        <?php if ((($produit->getPublier() == 0)) || (($produit->getPublier()) == NULL)) { ?>
            <input type="checkbox" id="publier" checked="checked" name="publier" value="0">

        <?php } else { ?>
            <input type="checkbox" id="publier" name="publier" value="1">

        <?php } ?>
        Brouillon
    </label>
    <input type="hidden" name="MAX_FILE_SIZE" value="500000" />

    <label class="btn btn-default btn-file">Upload image
        <input id="image" name="image" type="file" />
    </label>

    <p class="help-block">(format .jpg/.gif/.png)</p>

    <div class="form-group">
        <input class="btn btn-primary" name="ok" type="submit" value="VALIDER">
    </div>

</form>
