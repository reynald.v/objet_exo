<?php
if ($this->hasFlash() == true) {//on peut mettre this car notre vue est incluse gràce à la fonction render dans produitAction du catalogue controleur
    ?>
    <div class="alert alert-danger" style="background-color: pink;"><?php echo $this->getFlash(); ////on peut faire un echo de la fonction pour afficher le message vu que la fonction getFlash retourne le message                                                                       ?></div>
<?php } ?>
<h1>Admin Articles</h1>
<hr>
<a href="admin?module=produit&methode=ajout"><button type="button" class="btn btn-success">+ Ajouter</button></a>
<table class="table table-striped">
    <tr>
        <td><strong>#</strong></td>
        <td><strong>Date</strong></td>
        <td><strong>Titre</strong></td>
        <td class="contenu"><strong>Contenu</strong></td>
        <td class="text-center"><strong>Image</strong></td>
        <td><strong>Catégorie</strong></td>
        <td><strong>Auteur</strong></td>
        <td class="text-center"><strong>Action</strong></td>
    </tr>
    <?php
    foreach ($all_produits as $donnee) {
        if ($donnee->getPublier() == 0):
            ?>
            <tr class="redBg">
                <td><?php echo $donnee->getId(); ?></td>
                <td><?php echo $donnee->getFormatDate($donnee->getDate()->format('d-m-Y')); ?></td>
                <td><?php echo $donnee->titreProd; ?> <strong>BROUILLON</strong></td>
            <?php else: ?>
            <tr>
                <td><?php echo $donnee->getId(); ?></td>
                <td><?php echo $donnee->getFormatDate($donnee->getDate()->format('d-m-Y')); ?></td>
                <td><?php echo $donnee->titreProd; ?></td>
            <?php endif; ?>

            <td class="contenu"><?php echo $donnee->getExtrait($donnee->getContenu()); ?></td>
            <td class="text-center imageTd">
                <?php if ($donnee->miniature != null): ?>
                    <img class="img-thumbnail imageTab" src="<?php echo \Lib\Application::REP_IMAGES . "thumbnails/" . $donnee->miniature; ?>" alt="<?php echo htmlentities($donnee->alt, ENT_QUOTES); ?>">
                <?php endif; ?>
            </td>
            <td><?php echo $donnee->titreCat; ?></td>
            <td><?php echo $donnee->login; ?></td>
            <td>
                <a href="admin?module=produit&methode=update&id=<?php echo $donnee->getId(); ?>"><button type="button" class="btn btn-warning">Editer</button></a>
                <a href="admin?module=produit&methode=delete&id=<?php echo $donnee->getId(); ?>" onclick="return confirm('Sur ?');"><button type="button" class="btn btn-danger">Supprimer</button></a>

            </td>
        </tr>
        <?php
    }
    ?>

</table>

<ul class="pagination">
    <?php if ($page > 1) : ?>
        <li><a href="appBackend.php?module=produit&methode=index&page=<?php echo $page - 1; ?>">&larr;</a></li>
    <?php else: ?>
        <li class="paginationBtn paginationDebut"></li>
    <?php endif; ?>
    <?php for ($i = 1; $i <= $pages; $i++) : ?>
        <?php if ($page == $i) : ?>
            <li class="active"><a href="appBackend.php?module=produit&methode=index&page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php else: ?>
            <li><a href="appBackend.php?module=produit&methode=index&page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php endif; ?>
    <?php endfor; ?>
    <?php if ($page < $pages) : ?>
        <li><a href="appBackend.php?module=produit&methode=index&page=<?php echo $page + 1; ?>">&rarr;</a></li>
    <?php else: ?>
        <li class="paginationBtn paginationFin"></li>
        <?php endif; ?>
</ul>

