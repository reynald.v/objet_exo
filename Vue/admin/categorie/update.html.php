<?php
if ($this->hasFlash() == true) {//on peut mettre this car notre vue est incluse gràce à la fonction render dans produitAction du catalogue controleur
    ?>
    <?php echo $this->getFlash(); ////on peut faire un echo de la fonction pour afficher le message vu que la fonction getFlash retourne le message   ?>
<?php } ?>
<h1>Update Categorie</h1>

<div class="form-group">
    <form method="post" enctype="multipart/form-data">

        <fieldset>
            <legend>Mettre à jour Categorie</legend>

            <div class="form-group">
                <label for="titre">Titre</label>
                <input class="form-control" id="titre" name="titre" type="text" placeholder="Le titre" value="<?php echo $categorie->getTitre(); ?>"/>
            </div>


        </fieldset>

        <div class="form-group">
            <input class="btn btn-primary" name="ok" type="submit" value="VALIDER">
        </div>

    </form>
</div>