<?php
if ($this->hasFlash() == true) {//on peut mettre this car notre vue est incluse gràce à la fonction render dans produitAction du catalogue controleur
    ?>
    <div class="alert alert-danger" style="background-color: pink;"><?php echo $this->getFlash(); ////on peut faire un echo de la fonction pour afficher le message vu que la fonction getFlash retourne le message    ?></div>
<?php } ?>
<div  style="border-radius: 5px;background-color: #cccccc;padding: 10px;max-width:50%;margin:0 auto;">
    <h1 style="text-align: center;padding:0;">CONNEXION</h1>
    <form method="post" class="form-horizontal">
        <input type="hidden" value="<?php echo $token; ?>" name="token" />
        <div class="form-group">
            <label>Votre Nom <input class="form-control" type="text" name="login"/></label>
        </div>
        <div class="form-group">
            <label>Votre mot de passe <input class="form-control" type="password" name="pwd"/></label>
        </div>
        <div class="form-group">
            <button type="submit" name="submit" class="btn btn-default" style="width: 215px;"/>Se connecter</button>
        </div>
    </form>
</div>