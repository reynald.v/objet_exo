<?php
if ($this->hasFlash() == true) {//on peut mettre this car notre vue est incluse gràce à la fonction render dans produitAction du catalogue controleur
    ?>
    <?php echo $this->getFlash(); ////on peut faire un echo de la fonction pour afficher le message vu que la fonction getFlash retourne le message            ?>
<?php } ?>
<h1>Ajout Article</h1>

<div class="form-group">
    <form method="post" enctype="multipart/form-data">

        <fieldset>
            <legend>Remplissez le Nouvel Article</legend>

            <div class="form-group">
                <label for="titre">Titre</label>
                <input class="form-control" id="titre" name="titre" type="text" placeholder="Le titre">
            </div>

            <div class="form-group">
                <label for="contenu">contenu</label>
                <textarea class="form-control ckeditor" id="contenu" name="contenu" placeholder="Votre contenu"></textarea>
            </div>
            <!--
            <div class="form-group">

                            <label>
                                <input type="checkbox" id="publier" name="publier" value="0">
                                Brouillon
                            </label>
                        </div>-->

        </fieldset>
        <!--        <div class="form-group">
                    <label for="image">Image</label>
                    <input class="form-control" id="imageTEST" name="image" type="text" placeholder="Image">
                </div>-->
        <fieldset>


            <div class="form-group">
                <input type="hidden" name="MAX_FILE_SIZE" value="500000" />

                <label class="btn btn-default btn-file">Upload image
                    <input id="image" name="image" type="file"/>
                </label>

                <p class="help-block">(format .jpg/.gif/.png)</p>
            </div>
        </fieldset>
        <div class="form-group">
            <input class="btn btn-primary" name="ok" type="submit" value="VALIDER">
        </div>

    </form>
</div>