<h1>Admin Articles</h1>
<hr>
<a href="admin?module=article&methode=ajout"><button type="button" class="btn btn-success">+ Ajouter</button></a>
<table class="table table-striped">
    <tr>
        <td><strong>#</strong></td>

        <td><strong>Titre</strong></td>
        <td class="contenu"><strong>Contenu</strong></td>
        <td class="text-center"><strong>Image</strong></td>
        <td><strong>Auteur</strong></td>

        <td><strong>Date</strong></td>
        <td class="text-center"><strong>Action</strong></td>
    </tr>
    <?php
    foreach ($all_articles as $donnee) {
        ?>

        <?php if ($donnee->getPublier() == 0) { ?>
            <tr class="redBg">
                <td><?php echo $donnee->getId(); ?></td>
                <td><?php echo $donnee->getTitre(); ?></br><strong>BROUILLON</strong></td>
            <?php } else { ?>
            <tr>
                <td><?php echo $donnee->getId(); ?></td>
                <td><?php echo $donnee->getTitre(); ?></td>
            <?php } ?>

            <td class="contenu"><?php echo $donnee->getExtrait($donnee->getContenu()); ?></td>
            <td class="text-center imageTd">
                <?php if ($donnee->getImage() != null): ?>
                    <img class="img-thumbnail imageTab" src="<?php echo \Lib\Application::REP_IMAGES . 'thumbnails/' . $donnee->getImage(); ?>" alt="<?php echo htmlentities($donnee->getTitre(), ENT_QUOTES); ?>">
                <?php endif; ?>
            </td>
            <td>

                <?php echo $donnee->login; ?>

            </td>

            <td><?php echo $donnee->getFormatDate($donnee->getDate()->format('d-m-Y')); ?></td>
            <td>
                <a href="admin?module=article&methode=update&id=<?php echo $donnee->getId(); ?>"><button type="button" class="btn btn-warning">Editer</button></a>
                <a href="admin?module=article&methode=delete&id=<?php echo $donnee->getId(); ?>" onclick="return confirm('Sur ?');"><button type="button" class="btn btn-danger">Supprimer</button></a>

            </td>
        </tr>
        <?php
    }
    ?>

</table>

<ul class="pagination">
    <?php if ($page > 1) : ?>
        <li><a href="?page=<?php echo $page - 1; ?>">&larr;</a></li>
    <?php else: ?>
        <li class="paginationBtn paginationDebut"></li>
    <?php endif; ?>
    <?php for ($i = 1; $i <= $pages; $i++) : ?>
        <?php if ($page == $i) : ?>
            <li class="active"><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php else: ?>
            <li><a href="?page=<?php echo $i; ?>"><?php echo $i; ?></a></li>
        <?php endif; ?>
    <?php endfor; ?>
    <?php if ($page < $pages) : ?>
        <li><a href="?page=<?php echo $page + 1; ?>">&rarr;</a></li>
    <?php else: ?>
        <li class="paginationBtn paginationFin"></li>
        <?php endif; ?>
</ul>

