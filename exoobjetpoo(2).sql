-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Dim 17 Décembre 2017 à 22:55
-- Version du serveur :  5.7.9
-- Version de PHP :  5.6.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `exoobjetpoo`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `slug` varchar(200) NOT NULL,
  `titre` varchar(200) NOT NULL,
  `contenu` text NOT NULL,
  `date` date NOT NULL,
  `auteur` int(11) UNSIGNED NOT NULL,
  `image` varchar(200) DEFAULT NULL,
  `image_originale` varchar(200) DEFAULT NULL,
  `publier` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`auteur`),
  KEY `user_id_2` (`auteur`),
  KEY `user_id_3` (`auteur`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`id`, `slug`, `titre`, `contenu`, `date`, `auteur`, `image`, `image_originale`, `publier`) VALUES
(101, 'qqmomodifddddd', 'qQmoMODIFddddd((((', 'qqQqQ', '2017-04-12', 1, '82f87bfcb39c89fe0e9ee6da49e6567095e318b0.png', '82f87bfcb39c89fe0e9ee6da49e6567095e318b0.jpg', 1),
(142, 'test-article-modif-2-17-12-2017', 'test article MODIF 2 17-12-2017', 'Phasellus efficitur vestibulum leo sed vestibulum. Etiam molestie tristique porta. Sed auctor eleifend urna, in lobortis odio eleifend id. Duis bibendum dui sit amet urna pretium maximus vitae at nisl. Aliquam ut eros libero. Nunc egestas nulla orci, ut fermentum sapien blandit ut. Suspendisse elementum scelerisque ante, vel dictum nunc aliquet quis. In hac habitasse platea dictumst. Curabitur lobortis arcu sed enim pretium scelerisque. Sed orci neque, condimentum sit amet tincidunt id, sagittis id metus.', '2017-12-17', 1, '6f5a74a667aa0e271715aed8b390d3255fd54dc7.png', '6f5a74a667aa0e271715aed8b390d3255fd54dc7.jpg', 0),
(143, 'test-de-18-04', 'test de 18-04', 'dsdssd', '2017-04-18', 1, '89f3d45da749f6f0b5a760e2cb846765e836094a.png', '89f3d45da749f6f0b5a760e2cb846765e836094a.jpg', 1),
(144, 'test-ajout-article-modif-13h54', 'test ajout article MODIF 13h54', 'sss', '2017-04-18', 1, '6a49aa7c373b51d885ad6744fb323e8612e62265.png', '6a49aa7c373b51d885ad6744fb323e8612e62265.jpg', 1),
(146, '123h59', '123h59', 'sss', '2017-04-18', 1, 'c10fffa1eea9a52fdcefc95939bd0eda0abc4948.png', 'c10fffa1eea9a52fdcefc95939bd0eda0abc4948.jpg', 1),
(147, 'dcccc', 'dcccc', 'cccc', '2017-04-18', 1, 'f30790be8f25bf710fadcdf47952430b82ba1c9e.png', 'f30790be8f25bf710fadcdf47952430b82ba1c9e.jpg', 1),
(148, 'nouveau', 'nouveau', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed laoreet dui at leo pulvinar blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec vel magna eget massa aliquet vestibulum eget et tortor. Sed hendrerit magna nec enim ultricies consequat. Vestibulum fermentum vulputate dui eget ornare. Nam eget aliquet est. Aliquam vel iaculis risus, at pellentesque nulla. Pellentesque eleifend aliquet mattis. Pellentesque ut ex nisi. Duis feugiat at metus ac sollicitudin. Curabitur velit quam, semper sit amet urna id, pulvinar sagittis dui. In tincidunt lorem ac ex interdum sodales. Vestibulum erat sapien, molestie a velit at, porta gravida lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-04-23', 1, 'b96356ef468101a6f1ccf38d0a17b7ffc535e53d.png', 'b96356ef468101a6f1ccf38d0a17b7ffc535e53d.png', 1),
(149, 'new', 'new', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed laoreet dui at leo pulvinar blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec vel magna eget massa aliquet vestibulum eget et tortor. Sed hendrerit magna nec enim ultricies consequat. Vestibulum fermentum vulputate dui eget ornare. Nam eget aliquet est. Aliquam vel iaculis risus, at pellentesque nulla. Pellentesque eleifend aliquet mattis. Pellentesque ut ex nisi. Duis feugiat at metus ac sollicitudin. Curabitur velit quam, semper sit amet urna id, pulvinar sagittis dui. In tincidunt lorem ac ex interdum sodales. Vestibulum erat sapien, molestie a velit at, porta gravida lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-04-23', 1, '390e0a2cdf2069f17c0d6b63c35f52ed528c2be8.png', '390e0a2cdf2069f17c0d6b63c35f52ed528c2be8.jpg', 1),
(150, 'new2', 'new2', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed laoreet dui at leo pulvinar blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec vel magna eget massa aliquet vestibulum eget et tortor. Sed hendrerit magna nec enim ultricies consequat. Vestibulum fermentum vulputate dui eget ornare. Nam eget aliquet est. Aliquam vel iaculis risus, at pellentesque nulla. Pellentesque eleifend aliquet mattis. Pellentesque ut ex nisi. Duis feugiat at metus ac sollicitudin. Curabitur velit quam, semper sit amet urna id, pulvinar sagittis dui. In tincidunt lorem ac ex interdum sodales. Vestibulum erat sapien, molestie a velit at, porta gravida lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', '2017-04-23', 1, 'aa79a64a35e5a66faf3beae10ea936234fc40c75.png', 'aa79a64a35e5a66faf3beae10ea936234fc40c75.png', 1);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `categorie`
--

INSERT INTO `categorie` (`id`, `titre`, `slug`) VALUES
(1, 'Categorie1 MODIF', 'categorie1-modif'),
(2, 'Categorie2', 'slugCategorie2'),
(3, 'TEST AJOUT 22h16', 'test-ajout-22h16'),
(4, 'zzdzdz MODIFIIF', 'zzdzdz-modifiif');

-- --------------------------------------------------------

--
-- Structure de la table `image`
--

DROP TABLE IF EXISTS `image`;
CREATE TABLE IF NOT EXISTS `image` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `alt` varchar(255) NOT NULL,
  `miniature` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=400 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `image`
--

INSERT INTO `image` (`id`, `url`, `alt`, `miniature`) VALUES
(1, '05df4993f310d6c60729755d31cec3bd40c46180.png', '05df4993f310d6c60729755d31cec3bd40c46180-png', '05df4993f310d6c60729755d31cec3bd40c46180.png'),
(366, 'a84c7c1b766133e2cfe0cb56f59c15c31ed34310.jpg', 'a84c7c1b766133e2cfe0cb56f59c15c31ed34310-jpg', 'a84c7c1b766133e2cfe0cb56f59c15c31ed34310.png'),
(373, 'e43787d16f3cc8fe01a13286f94337c60d842988.jpg', 'e43787d16f3cc8fe01a13286f94337c60d842988-jpg', 'e43787d16f3cc8fe01a13286f94337c60d842988.png'),
(387, 'a741c30ef03741534732bb9fa9b53de0bbb04dfb.png', 'a741c30ef03741534732bb9fa9b53de0bbb04dfb-png', 'a741c30ef03741534732bb9fa9b53de0bbb04dfb.png'),
(390, '06329e11c219b0a67143079489c5b5b5d43e9a36.png', '06329e11c219b0a67143079489c5b5b5d43e9a36-png', '06329e11c219b0a67143079489c5b5b5d43e9a36.png'),
(391, 'f18b0b956c38dea19ee5a7996ccd12361b226a1f.png', 'f18b0b956c38dea19ee5a7996ccd12361b226a1f-png', 'f18b0b956c38dea19ee5a7996ccd12361b226a1f.png'),
(392, 'd34a664a5f3f7b2572277af0ff24ceddef184867.png', 'd34a664a5f3f7b2572277af0ff24ceddef184867-png', 'd34a664a5f3f7b2572277af0ff24ceddef184867.png'),
(393, '920f231b2049ae1dddebf628b388b6b4c716875e.png', '920f231b2049ae1dddebf628b388b6b4c716875e-png', '920f231b2049ae1dddebf628b388b6b4c716875e.png'),
(394, '1d41611ae9cbf151b32d5184c6acef5cfeb036c4.png', '1d41611ae9cbf151b32d5184c6acef5cfeb036c4-png', '1d41611ae9cbf151b32d5184c6acef5cfeb036c4.png'),
(395, '28a991bac350f6bc589587ff3a220c0e312e68d3.png', '28a991bac350f6bc589587ff3a220c0e312e68d3-png', '28a991bac350f6bc589587ff3a220c0e312e68d3.png'),
(396, '59b7ad2922f94a7487d8661014c68879040e2922.png', '59b7ad2922f94a7487d8661014c68879040e2922-png', '59b7ad2922f94a7487d8661014c68879040e2922.png'),
(397, '5b9b45fccfd1fed0c4c3df8b56aab63de7fb1adc.jpg', '5b9b45fccfd1fed0c4c3df8b56aab63de7fb1adc-jpg', '5b9b45fccfd1fed0c4c3df8b56aab63de7fb1adc.png'),
(398, '3e9b20af98ab08f5bdff00026964757c2af0fdd1.jpg', '3e9b20af98ab08f5bdff00026964757c2af0fdd1-jpg', '3e9b20af98ab08f5bdff00026964757c2af0fdd1.png'),
(399, 'af334695c8b1d8d09e07d172661d40c108195352.jpg', 'af334695c8b1d8d09e07d172661d40c108195352-jpg', 'af334695c8b1d8d09e07d172661d40c108195352.png');

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

DROP TABLE IF EXISTS `notes`;
CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `auteur` varchar(60) NOT NULL,
  `valeur` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `produit` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`),
  KEY `produit` (`produit`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `notes`
--

INSERT INTO `notes` (`id`, `auteur`, `valeur`, `date`, `produit`) VALUES
(2, 'qssss', 4, '2017-04-23', 183);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

DROP TABLE IF EXISTS `produit`;
CREATE TABLE IF NOT EXISTS `produit` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `titre` varchar(255) NOT NULL,
  `contenu` text NOT NULL,
  `image` int(10) UNSIGNED DEFAULT NULL,
  `prix` decimal(10,0) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `categorie` int(11) UNSIGNED DEFAULT NULL,
  `publier` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `categorie` (`categorie`),
  KEY `categorie_2` (`categorie`),
  KEY `image` (`image`)
) ENGINE=InnoDB AUTO_INCREMENT=195 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`id`, `date`, `titre`, `contenu`, `image`, `prix`, `slug`, `categorie`, `publier`) VALUES
(183, '2017-04-12', 'dcdcdcMODDIF2222', 'xcxcxcxc', 366, NULL, 'dcdcdcmoddif2222', 1, 1),
(184, '2017-12-12', 'hjgfgfgfMO', 'vvv', 373, NULL, 'hjgfgfgfmo', 1, 1),
(187, '2017-12-18', 'eeeeeMODIFMODIF', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed laoreet dui at leo pulvinar blandit. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec vel magna eget massa aliquet vestibulum eget et tortor. Sed hendrerit magna nec enim ultricies consequat. Vestibulum fermentum vulputate dui eget ornare. Nam eget aliquet est. Aliquam vel iaculis risus, at pellentesque nulla. Pellentesque eleifend aliquet mattis. Pellentesque ut ex nisi. Duis feugiat at metus ac sollicitudin. Curabitur velit quam, semper sit amet urna id, pulvinar sagittis dui. In tincidunt lorem ac ex interdum sodales. Vestibulum erat sapien, molestie a velit at, porta gravida lacus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.', 387, NULL, 'eeeeemodifmodif', 1, 1),
(190, '2017-12-10', 'rzrzrrz', 'zrzzrzrzr', 390, NULL, 'rzrzrrz', 1, 1),
(191, '2017-12-17', 'sqsqsqssq MODIF DATE', 'qssqsqqssqs', 391, NULL, 'sqsqsqssq-modif-date', 2, 1),
(192, '2017-12-17', '21h12 MODIF', 'Phasellus efficitur vestibulum leo sed vestibulum. Etiam molestie tristique porta. Sed auctor eleifend urna, in lobortis odio eleifend id. Duis bibendum dui sit amet urna pretium maximus vitae at nisl. Aliquam ut eros libero. Nunc egestas nulla orci, ut fermentum sapien blandit ut. Suspendisse elementum scelerisque ante, vel dictum nunc aliquet quis. In hac habitasse platea dictumst. Curabitur lobortis arcu sed enim pretium scelerisque. Sed orci neque, condimentum sit amet tincidunt id, sagittis id metus.', 392, NULL, '21h12-modif', 1, 1),
(193, '2017-12-17', 'TEST PRODUIT 17-12 22h46', 'Phasellus efficitur vestibulum leo sed vestibulum. Etiam molestie tristique porta. Sed auctor eleifend urna, in lobortis odio eleifend id. Duis bibendum dui sit amet urna pretium maximus vitae at nisl. Aliquam ut eros libero. Nunc egestas nulla orci, ut fermentum sapien blandit ut. Suspendisse elementum scelerisque ante, vel dictum nunc aliquet quis. In hac habitasse platea dictumst. Curabitur lobortis arcu sed enim pretium scelerisque. Sed orci neque, condimentum sit amet tincidunt id, sagittis id metus.', 398, NULL, 'test-produit-17-12-22h46', 1, 0),
(194, '2017-12-17', 'test ajout brouillon', 'Phasellus efficitur vestibulum leo sed vestibulum. Etiam molestie tristique porta. Sed auctor eleifend urna, in lobortis odio eleifend id. Duis bibendum dui sit amet urna pretium maximus vitae at nisl. Aliquam ut eros libero. Nunc egestas nulla orci, ut fermentum sapien blandit ut. Suspendisse elementum scelerisque ante, vel dictum nunc aliquet quis. In hac habitasse platea dictumst. Curabitur lobortis arcu sed enim pretium scelerisque. Sed orci neque, condimentum sit amet tincidunt id, sagittis id metus.', 399, NULL, 'test-ajout-brouillon', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `login` varchar(40) NOT NULL,
  `password` varchar(255) NOT NULL,
  `privilege` tinyint(4) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `login`, `password`, `privilege`, `email`) VALUES
(1, 'Reynald', '$2y$10$fHzUJdd201V3GHulj5RXrewfPsr2fCrX8q768mAGvuy9gc0L3m/HC', 4, 'reynald.v@free.fr'),
(2, 'duchmol', '$2y$10$fHzUJdd201V3GHulj5RXrewfPsr2fCrX8q768mAGvuy9gc0L3m/HC', 1, 'machin@free.fr');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `article_ibfk_1` FOREIGN KEY (`auteur`) REFERENCES `user` (`id`) ON UPDATE CASCADE;

--
-- Contraintes pour la table `notes`
--
ALTER TABLE `notes`
  ADD CONSTRAINT `notes_ibfk_1` FOREIGN KEY (`produit`) REFERENCES `produit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`categorie`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `produit_ibfk_2` FOREIGN KEY (`image`) REFERENCES `image` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
